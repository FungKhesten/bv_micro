package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.AccountType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountTypeRepository extends CrudRepository<AccountType, Long> {

//    AccountType findByNameAndOrgId(String name, long orgId);
//    AccountType findByNumberAndOrgId(String number, long orgId);

//    List<AccountType> findByOrgId(long orgId);
//    List<AccountType> findByOrgIdAndCategory(long orgId, String category);

    AccountType findByNameAndOrgIdAndActiveTrue(String name, long orgId);
    AccountType findByNumberAndOrgIdAndActiveTrue(String number, long orgId);

    List<AccountType> findByOrgIdAndActiveTrue(long orgId);

    List<AccountType> findByOrgIdAndName(long orgId, String name);

    List<AccountType> findByOrgId(long orgId);

    List<AccountType> findByOrgIdAndCategoryAndActiveTrue(long orgId, String category);


}

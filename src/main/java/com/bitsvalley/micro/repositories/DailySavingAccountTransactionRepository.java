package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.DailySavingAccountTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DailySavingAccountTransactionRepository extends CrudRepository<DailySavingAccountTransaction, Long> {

    List<DailySavingAccountTransaction> findByDailySavingAccount(String savingAccount);

    Optional<DailySavingAccountTransaction> findByReferenceAndOrgId(String reference, long orgId);

    @Query(value = "SELECT * FROM daily_saving_account_transaction ca WHERE ca.created_date BETWEEN :startDate AND :endDate AND ca.org_id = :orgId", nativeQuery = true)
    List<DailySavingAccountTransaction> searchStartEndDate(String startDate, String endDate, long orgId);

    @Query(value = "SELECT * FROM daily_saving_account_transaction ca WHERE ca.created_date BETWEEN :startDate AND :endDate AND ca.created_by = :userName AND ca.org_id = :orgId", nativeQuery = true)
    List<DailySavingAccountTransaction> searchStartEndDate(String startDate, String endDate, String userName, long orgId);

}

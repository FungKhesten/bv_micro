package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.DailySavingAccount;
import com.bitsvalley.micro.domain.SavingAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DailySavingAccountRepository extends CrudRepository<DailySavingAccount, Long> {

    DailySavingAccount findByAccountNumberAndOrgId(String accountNumber, long orgId);

    @Query(value = "SELECT COUNT(*) AS numberOfSavingAccount FROM DailySavingAccount sa where sa.branchCode = :branchCode AND sa.orgId = :orgId")
    int countNumberOfProductsCreatedInBranch(String branchCode, long orgId);

}

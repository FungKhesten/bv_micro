package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.Invoice;
import com.bitsvalley.micro.domain.OrderItem;
import com.bitsvalley.micro.utils.OrderType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {

}

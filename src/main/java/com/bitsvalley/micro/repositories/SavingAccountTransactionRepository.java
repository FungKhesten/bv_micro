package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.SavingAccountTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SavingAccountTransactionRepository extends CrudRepository<SavingAccountTransaction, Long> {

    List<SavingAccountTransaction> findBySavingAccount(String savingAccount);

    Optional<SavingAccountTransaction> findByReferenceAndOrgId(String reference, long orgId);

    @Query(value = "SELECT * FROM saving_account_transaction ca WHERE ca.created_date BETWEEN :startDate AND :endDate AND ca.org_id = :orgId", nativeQuery = true)
    List<SavingAccountTransaction> searchStartEndDate(String startDate, String endDate, long orgId);

    @Query(value = "SELECT * FROM saving_account_transaction ca WHERE ca.created_date BETWEEN :startDate AND :endDate AND saving_account_id = :id AND ca.org_id = :orgId", nativeQuery = true)
    List<SavingAccountTransaction> searchStartEndDateFilter(String startDate, String endDate, long id, long orgId);

    @Query(value = "SELECT * FROM saving_account_transaction ca WHERE ca.created_date BETWEEN :startDate AND :endDate AND ca.created_by = :userName AND ca.org_id = :orgId", nativeQuery = true)
    List<SavingAccountTransaction> searchStartEndDate(String startDate, String endDate, String userName, long orgId);

//    @Query(value = "SELECT * FROM saving_account_transaction ca WHERE ca.created_date BETWEEN :startDate AND :endDate AND saving_account_id = :id", nativeQuery = true)
//    List<SavingAccountTransaction> searchStartEndDateFilter(String startDate, String endDate, long id);
//    @Query(value = "SELECT * FROM SAVING_ACCOUNT_TRANSACTION ca WHERE saving_account_id = :savingAccountId", nativeQuery = true)
//    List<SavingAccountTransaction> searchStartEndDateAccount(String startDate, String endDate, Long savingAccountId);

}

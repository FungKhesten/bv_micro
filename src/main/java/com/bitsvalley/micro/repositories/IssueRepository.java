package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.Issue;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IssueRepository extends CrudRepository<Issue, Long> {

    List<Issue> findByOrgId( long orgID);


}

package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.SavingAccountTransaction;
import com.bitsvalley.micro.domain.User;
import com.bitsvalley.micro.domain.UserRole;
import com.bitsvalley.micro.utils.AccountStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUserName(String userName);

    User findByUserNameAndOrgId(String userName, long orgId);

    ArrayList<User> findByOrgId(long orgId);

    ArrayList<User> findByOrgIdAndId(long orgId, long id);

    ArrayList<User> findByOrgIdAndAccountStatus(long orgId, AccountStatus status);

    ArrayList<User> findAllByUserRoleInAndOrgId(ArrayList<UserRole> userRole, Long orgId);

    ArrayList<User> findAllByUserRoleInAndOrgIdAndAccountStatus(ArrayList<UserRole> userRole, Long orgId, AccountStatus accountStatus);
//    ArrayList<User> findAllByUserRoleInAndOrgIdAndAccountStatus(ArrayList<UserRole> userRole, Long orgId, AccountStatus accountStatus);

    ArrayList<User> findDistintAllByUserRoleNotInAndOrgId(ArrayList<UserRole> userRole, Long orgId);

//    @Query(value = "SELECT count(*) FROM user la WHERE la.userRole IN (:role) AND la.orgId = :orgId", nativeQuery = true)
//    int countByUserRole(String role, long orgId);

//    @Query(value = "SELECT COUNT(*) AS numberOfLoanAccount FROM loanaccount la where la.branch_code = :branchCode and la.org_id = :orgId", nativeQuery = true)

}

package com.bitsvalley.micro.repositories;

import com.bitsvalley.micro.domain.Branch;
import com.bitsvalley.micro.domain.PaymentTransaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentTransactionRepository extends CrudRepository<PaymentTransaction, Long> {

    List<PaymentTransaction> findByOrgId( long orgID);

}

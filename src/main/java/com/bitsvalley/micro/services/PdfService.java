package com.bitsvalley.micro.services;


import com.bitsvalley.micro.domain.*;
import com.bitsvalley.micro.repositories.BranchRepository;
import com.bitsvalley.micro.repositories.UserRepository;
import com.bitsvalley.micro.utils.Amortization;
import com.bitsvalley.micro.utils.AmortizationRowEntry;
import com.bitsvalley.micro.utils.BVMicroUtils;
import com.bitsvalley.micro.webdomain.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.*;

@Service
public class PdfService {

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    public String generateSavingTransactionReceiptPDF(DailySavingAccountTransaction savingAccountTransaction, RuntimeSetting rt, boolean displayBalance) {
        String accountBalance = "";
        String accountDue = "";
        String representativeText = StringUtils.equals(savingAccountTransaction.getRepresentative(),BVMicroUtils.getFullName(savingAccountTransaction.getDailySavingAccount().getUser()))?"":"Remittance: "+savingAccountTransaction.getRepresentative();
        User aUser = userRepository.findByUserName(savingAccountTransaction.getCreatedBy());
        if (savingAccountTransaction.getAccountOwner() != null && StringUtils.equals("true", savingAccountTransaction.getAccountOwner()) && displayBalance) {
            accountBalance = BVMicroUtils.formatCurrency( savingAccountTransaction.getAccountBalance() ,rt.getCountryCode());

            double dueAmount = savingAccountTransaction.getDailySavingAccount().getAccountMinBalance() - savingAccountTransaction.getDailySavingAccount().getAccountBalance();
            if( dueAmount > 0 ){
                accountDue = "Balance Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) + " "+ rt.getCurrency()+"<br/>";
            }

        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String savingBilanzNoInterest = "<font style=\"font-size:1.4em;color:"+ rt.getThemeColor() +";\">" +
                "RECEIPT FOR <b>"+savingAccountTransaction.getDailySavingAccount().getAccountType().getDisplayName()+"</b> ACCOUNT TRANSACTION</font>" +
                "<table border=\"1\" width=\"100%\">" +
                "<tr> <td><table><tr><td>" +
                "<img width=\"80\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/> Reference No:<br/><b>"+ savingAccountTransaction.getReference() +
                "</b></td><td><b><font style=\"font-size:1.0em;color:"+ rt.getThemeColor() +";\"> "+ rt.getBusinessName() +"</font></b><br/><font style=\"font-size:0.7em;color:black;\"> "+rt.getSlogan()+"</font><br/>" + rt.getAddress()+"<br/>" +rt.getTelephone() +"<br/>" +rt.getEmail() +"<br/>" +
                "</td></tr></table></td>" +
                "<td>"+
                " Branch No: "+savingAccountTransaction.getBranchCode()+
                "<br/>"+savingAccountTransaction.getModeOfPayment()+": " + BVMicroUtils.formatCurrency(savingAccountTransaction.getSavingAmount(),rt.getCountryCode()) +"<br/> "+ representativeText +"<br/>Date: " + BVMicroUtils.formatDateTime(savingAccountTransaction.getCreatedDate()) + "</td></tr>" +
                "<tr><td>" +
                "Account Number:<b><br/>" + BVMicroUtils.getFormatAccountNumber(savingAccountTransaction.getDailySavingAccount().getAccountNumber())

                + "</b><br/>Customer: <b>"+ BVMicroUtils.getFullName(savingAccountTransaction.getDailySavingAccount().getUser())+

                "</b> </td>" +
                "<td>Account Balance: <b>" + accountBalance + "</b>"+rt.getCurrency()+"<br/>"+accountDue+"Transaction Amount: <font style=\"font-size:1.6em;color:black;\">"
                + BVMicroUtils.formatCurrency(savingAccountTransaction.getSavingAmount(),rt.getCountryCode()) + "</font> " +rt.getCurrency()+"</td></tr>" +
                "<tr><td style=\"width:50%;\">" +
                "<b>Agent Representative:</b> <br/>" + BVMicroUtils.getFullName(aUser) +"<br/><b>Notes:</b> <br/>"+savingAccountTransaction.getNotes()+"<br/><b>Amount in Letters:</b> <font color=\" "+ rt.getThemeColor() + "\" size=\"8px\">" +
                " <br/>"+                            savingAccountTransaction.getSavingAmountInLetters() + "</font></td>\n" +
                " <td><table  border=\"1\" width=\"100%\" class=\"center\">" +
                "              <tr>" +
                "                      <th colspan=\"2\"><font style=\"font-size:1.2em;color:"+ rt.getThemeColor() +";\">Bill Selection - Cash Breakdown</font></th>" +
                "               </tr>" +
                "                 <tr>" +
                "                 <td> 10 000 x " + savingAccountTransaction.getTenThousand() + " = <b>"  + BVMicroUtils.formatCurrency( 10000 * savingAccountTransaction.getTenThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 5 000 x " + savingAccountTransaction.getFiveThousand() + " = <b>" + BVMicroUtils.formatCurrency(  5000 * savingAccountTransaction.getFiveThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 2 000 x " + savingAccountTransaction.getTwoThousand() + " = <b>" + BVMicroUtils.formatCurrency( 2000 * savingAccountTransaction.getTwoThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 1 000 x" + savingAccountTransaction.getOneThousand() + " = <b>" + BVMicroUtils.formatCurrency(  1000 * savingAccountTransaction.getOneThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 500 x " + savingAccountTransaction.getFiveHundred() + " = <b>" + BVMicroUtils.formatCurrency( 500 * savingAccountTransaction.getFiveHundred(),rt.getCountryCode()) + "</b><br/>" +
                "                 100 x " + savingAccountTransaction.getOneHundred() + " = <b>" + BVMicroUtils.formatCurrency( 100 * savingAccountTransaction.getOneHundred(),rt.getCountryCode()) + "</b><br/>" +

                "</td><td>"+
                "                 50 x " + savingAccountTransaction.getFifty() + " = <b>" + 50 * savingAccountTransaction.getFifty() + "</b><br/>"  +
                "                 25 x " + savingAccountTransaction.getTwentyFive() + " = <b>" + 25 * savingAccountTransaction.getTwentyFive() + "</b><br/>" +
                "                 10 x " + savingAccountTransaction.getTen() + " = <b>" + 10 * savingAccountTransaction.getTen() + "</b><br/>" +
                "                 5 x " + savingAccountTransaction.getFive() + " = <b>" + 5 * savingAccountTransaction.getFive() + "</b><br/>" +
                "                 1 x " + savingAccountTransaction.getOne() + " = <b>" + 1 * savingAccountTransaction.getOne() + "</b><br/>" +

                "                  </td></tr>" +
                "                 </table></td></tr></table>" +
                "       <table><tr><td><br/><br/>Cashier Signature: ------------------------------------- Customer Signature: -------------------------------------<br/> "+branchRepository.findByCodeAndOrgId(savingAccountTransaction.getBranchCode(),savingAccountTransaction.getOrgId()).getName() +"</td>" +
                "</tr></table>";
        savingBilanzNoInterest = "<html><head></head><body>" + savingBilanzNoInterest  +
                "--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- <br/><br/>"+ savingBilanzNoInterest+"</body></html>";
        return savingBilanzNoInterest;
    }

    public String generateSavingTransactionReceiptPDF(SavingAccountTransaction savingAccountTransaction, RuntimeSetting rt, boolean printBalance) {
        String accountBalance = "";
        String accountDue = "";
        String representativeText = StringUtils.equals(savingAccountTransaction.getRepresentative(),BVMicroUtils.getFullName(savingAccountTransaction.getSavingAccount().getUser()))?"":"Remittance: "+savingAccountTransaction.getRepresentative();
        User aUser = userRepository.findByUserName(savingAccountTransaction.getCreatedBy());
        if (savingAccountTransaction.getAccountOwner() != null && StringUtils.equals("true", savingAccountTransaction.getAccountOwner()) && printBalance) {
            accountBalance = BVMicroUtils.formatCurrency(savingAccountTransaction.getAccountBalance(), rt.getCountryCode());
            double dueAmount = savingAccountTransaction.getSavingAccount().getAccountMinBalance() - savingAccountTransaction.getSavingAccount().getAccountBalance();
            if( dueAmount > 0 ){
                accountDue = "Balance Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) + " "+rt.getCurrency()+"<br/>";
            }
        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String savingBilanzNoInterest = "<font style=\"font-size:1.4em;color:black;\">" +
                "RECEIPT FOR <b>"+savingAccountTransaction.getSavingAccount().getAccountType().getDisplayName()+"</b> ACCOUNT TRANSACTION</font>" +
                "<table border=\"1\" width=\"100%\">" +
                "<tr> <td><table><tr><td>" +
                "<img width=\"75\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/> Reference No:<br/><b>"+ savingAccountTransaction.getReference() +
                "</b></td><td><b><font style=\"font-size:1.0em;color:black;\"> "+ rt.getBusinessName() +"</font></b><br/><font style=\"font-size:0.7em;color:black;\"> "+rt.getSlogan()+"</font><br/>" + rt.getAddress()+"<br/>" +rt.getTelephone() +"<br/>" +rt.getEmail() +"<br/>" +
                "</td></tr></table></td>" +
                "<td>"+
                " Branch No: "+savingAccountTransaction.getBranchCode()+
                "<br/>"+savingAccountTransaction.getModeOfPayment()+": " + BVMicroUtils.formatCurrency(savingAccountTransaction.getSavingAmount(),rt.getCountryCode()) +"<br/> "+ representativeText +"<br/>Date: " + BVMicroUtils.formatDateTime(savingAccountTransaction.getCreatedDate()) + "</td></tr>" +
                "<tr><td>" +
                "Account Number:<b><br/>" + BVMicroUtils.getFormatAccountNumber(savingAccountTransaction.getSavingAccount().getAccountNumber())

                + "</b><br/>Customer: <b>"+ BVMicroUtils.getFullName(savingAccountTransaction.getSavingAccount().getUser())+

                "</b> </td>" +
                "<td>Account Balance: <b>" + accountBalance + "</b>"+rt.getCurrency()+"<br/>"+accountDue+"Transaction Amount: <font style=\"font-size:1.6em;color:black;\">"
                + BVMicroUtils.formatCurrency(savingAccountTransaction.getSavingAmount(),rt.getCountryCode()) + "</font>"+rt.getCurrency()+"</td></tr>" +
                "<tr><td style=\"width:50%;\">" +
                "<b>Agent Representative:</b> <br/>" + BVMicroUtils.getFullName(aUser) +"<br/><b>Notes:</b> <br/>"+savingAccountTransaction.getNotes()+"<br/><b>Amount in Letters:</b> <font color=\" "+ rt.getThemeColor() + "\" size=\"8px\">" +
                " <br/>"+                            savingAccountTransaction.getSavingAmountInLetters() + "</font></td>\n" +
                " <td><table  border=\"1\" width=\"100%\" class=\"center\">" +
                "              <tr>" +
                "                      <th colspan=\"2\"><font style=\"font-size:1.2em;color:black;\">Bill Selection - Cash Breakdown</font></th>" +
                "               </tr>" +
                "                 <tr>" +
                "                 <td> 10 000 x " + savingAccountTransaction.getTenThousand() + " = <b>"  + BVMicroUtils.formatCurrency( 10000 * savingAccountTransaction.getTenThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 5 000 x " + savingAccountTransaction.getFiveThousand() + " = <b>" + BVMicroUtils.formatCurrency(  5000 * savingAccountTransaction.getFiveThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 2 000 x " + savingAccountTransaction.getTwoThousand() + " = <b>" + BVMicroUtils.formatCurrency( 2000 * savingAccountTransaction.getTwoThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 1 000 x" + savingAccountTransaction.getOneThousand() + " = <b>" + BVMicroUtils.formatCurrency(  1000 * savingAccountTransaction.getOneThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 500 x " + savingAccountTransaction.getFiveHundred() + " = <b>" + BVMicroUtils.formatCurrency( 500 * savingAccountTransaction.getFiveHundred(),rt.getCountryCode()) + "</b><br/>" +
                "                 100 x " + savingAccountTransaction.getOneHundred() + " = <b>" + BVMicroUtils.formatCurrency( 100 * savingAccountTransaction.getOneHundred(),rt.getCountryCode()) + "</b><br/>" +

                "</td><td>"+
                "                 50 x " + savingAccountTransaction.getFifty() + " = <b>" + 50 * savingAccountTransaction.getFifty() + "</b><br/>"  +
                "                 25 x " + savingAccountTransaction.getTwentyFive() + " = <b>" + 25 * savingAccountTransaction.getTwentyFive() + "</b><br/>" +
                "                 10 x " + savingAccountTransaction.getTen() + " = <b>" + 10 * savingAccountTransaction.getTen() + "</b><br/>" +
                "                 5 x " + savingAccountTransaction.getFive() + " = <b>" + 5 * savingAccountTransaction.getFive() + "</b><br/>" +
                "                 1 x " + savingAccountTransaction.getOne() + " = <b>" + 1 * savingAccountTransaction.getOne() + "</b><br/>" +

                "                  </td></tr>" +
                "                 </table></td></tr></table>" +
                "       <table><tr><td><br/><br/>Cashier Signature: ------------------------------------- Customer Signature: -------------------------------------<br/> "+branchRepository.findByCodeAndOrgId(savingAccountTransaction.getSavingAccount().getUser().getBranch().getCode(),savingAccountTransaction.getOrgId()).getName() +"</td>" +
                "</tr></table>";
        savingBilanzNoInterest = "<html><head></head><body>" + savingBilanzNoInterest  +
                "--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- <br/><br/>"+ savingBilanzNoInterest+"</body></html>";
        return savingBilanzNoInterest;
    }



    public String generateShareDetailsPDF(ShareAccountTransaction shareAccountTransaction, RuntimeSetting rt) {
        User aUser = userRepository.findByUserName(shareAccountTransaction.getCreatedBy());

        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String shareBilanzNoInterest = "<html><head>" +
                "</head><body><br/><br/><font style=\"font-size:1.4em;color:black;\">" +
                "<b>RECEIPT FOR SHARE ACCOUNT TRANSACTION</b></font>" +
                "<table border=\"1\" width=\"100%\">" +
                "<tr> <td><img width=\"75\" src=\""+prefix+rt.getUnionLogo() + "\"/><br/> Reference No:" + shareAccountTransaction.getReference() +
                "<br/>Date:<b>" + BVMicroUtils.formatDateTime(shareAccountTransaction.getCreatedDate()) + "</b> </td>" +
                "<td>" +
                "<b><font style=\"font-size:1.6em;color:black;\"> " + rt.getBusinessName() + "</font></b><br/> Branch No: " + shareAccountTransaction.getBranchCode() +
                "<br/>Address:" + rt.getAddress() + "<br/> Telephone:" + rt.getTelephone() +
                "<br/>" + shareAccountTransaction.getModeOfPayment() + " Current Account" + BVMicroUtils.formatCurrency(shareAccountTransaction.getShareAmount(),rt.getCountryCode()) + "</td></tr>" +
                "<tr><td>" +
                "Account Number: " + BVMicroUtils.getFormatAccountNumber(shareAccountTransaction.getShareAccount().getAccountNumber())
                + "<br/>Customer: <b>" + shareAccountTransaction.getShareAccount().getUser().getLastName() + ","
                + shareAccountTransaction.getShareAccount().getUser().getFirstName() + "</b> </td>" +
                "<td>Total Share Balance: <b>" + BVMicroUtils.formatCurrency(shareAccountTransaction.getShareAccount().getAccountBalance(),rt.getCountryCode()) + "</b><br/> Current Amount:<font style=\"font-size:1.6em;color:black;\">"
                + BVMicroUtils.formatCurrency(shareAccountTransaction.getShareAmount(),rt.getCountryCode()) + "</font></td></tr>" +
                "        <tr><td>" +
                "Representative: <b>" + shareAccountTransaction.getCreatedBy() + "</b> - "+ BVMicroUtils.getFullName(aUser) +"<br/> </td><td> <font color=\"" + rt.getThemeColor() + "\" size=\"8px\"> "+
                "</font><br/>Notes:"+shareAccountTransaction.getNotes()+"</td>\n" +
                "    </tr></table>" +
                "    <table  border=\"1\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th><font style=\"font-size:1.2em;color:black;\">Bill Selection - Cash Breakdown</font><font style=\"font-size:1.6em;color:black;\"> " + BVMicroUtils.formatCurrency(shareAccountTransaction.getShareAmount(),rt.getCountryCode()) +" " +rt.getCurrency()+"</font></th>\n" +
                "            </tr>\n" +
                "        </table>" +
                "<br/> " +
                "       <table>" +
                "       <tr><td>Cashier Signature: ------------------------------ Customer Signature: ------------------------------<br/>  Bamenda Branch, N W Region,</td>" +
                "       <td></td></tr>" +
                "       </table>" +
                "<br/><br/>" +
                "" +
                "</body></html>";
        return shareBilanzNoInterest;
    }

    public String generateCurrentTransactionReceiptPDF(CurrentAccountTransaction currentAccountTransaction, RuntimeSetting rt, boolean displayBalance) {

        String accountBalance = "";
        String accountDue = "";

        String representativeText = StringUtils.equals(currentAccountTransaction.getRepresentative(),BVMicroUtils.getFullName(currentAccountTransaction.getCurrentAccount().getUser()))?"":"Remittance: "+currentAccountTransaction.getRepresentative();
        User aUser = userRepository.findByUserName(currentAccountTransaction.getCreatedBy());
        if (currentAccountTransaction.getAccountOwner() != null && StringUtils.equals("true", currentAccountTransaction.getAccountOwner()) && displayBalance) {
            accountBalance = BVMicroUtils.formatCurrency(currentAccountTransaction.getAccountBalance(),rt.getCountryCode());
            double dueAmount = currentAccountTransaction.getCurrentAccount().getAccountMinBalance() - currentAccountTransaction.getCurrentAccount().getAccountBalance();
            if( dueAmount > 0 ){
                accountDue = "Account Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) +" " +rt.getCurrency()+"<br/>";
            }
        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String currentBilanzNoInterest = "<font style=\"font-size:1.2em;color:black;\">" +
                "RECEIPT FOR <b>"+currentAccountTransaction.getCurrentAccount().getAccountType().getDisplayName()+"</b> ACCOUNT TRANSACTION</font>" +
                "<table border=\"1\" width=\"100%\">" +
                "<tr> <td><table><tr><td>" +
                "<img width=\"75\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/> Reference No:<br/><b>"+ currentAccountTransaction.getReference() +
                "</b></td><td><b><font style=\"font-size:1.4em;color:black;\"> "+ rt.getBusinessName() +"</font></b><br/><font style=\"font-size:0.7em;color:black;\"> "+rt.getSlogan()+"</font><br/>" + rt.getAddress()+"<br/>" +rt.getTelephone() +"<br/>" +rt.getEmail() +"<br/>" +
                "</td></tr></table></td>" +
                "<td>"+
                " Branch No: "+currentAccountTransaction.getBranchCode()+
                "<br/>"+currentAccountTransaction.getModeOfPayment()+": " + BVMicroUtils.formatCurrency(currentAccountTransaction.getCurrentAmount(),rt.getCountryCode()) +"<br/> "+ representativeText +"<br/>Date: " + BVMicroUtils.formatDateTime(currentAccountTransaction.getCreatedDate()) + "</td></tr>" +
                "<tr><td>" +
                "Account Number:<b><br/>" + BVMicroUtils.getFormatAccountNumber(currentAccountTransaction.getCurrentAccount().getAccountNumber())

                + "</b><br/>Customer: <b>"+ BVMicroUtils.getFullName(currentAccountTransaction.getCurrentAccount().getUser())+

                "</b> </td>" +
                "<td>Account Balance: <b>" + accountBalance + " </b> "  +rt.getCurrency()+"<br/>"+accountDue+"Transaction Amount: <font style=\"font-size:1.6em;color:black;\">"
                + BVMicroUtils.formatCurrency(currentAccountTransaction.getCurrentAmount(),rt.getCountryCode()) + " </font> "+rt.getCurrency()+"</td></tr>" +
                "<tr><td style=\"width:50%;\">" +
                "<b>Agent Representative:</b> <br/>" + BVMicroUtils.getFullName(aUser) +"<br/><b>Notes:</b> <br/>"+currentAccountTransaction.getNotes()+"<br/><b>Amount in Letters:</b> <font color=\" "+ rt.getThemeColor() + "\" size=\"8px\">" +
                " <br/>"+                            currentAccountTransaction.getCurrentAmountInLetters() + "</font></td>\n" +
                " <td><table  border=\"1\" width=\"100%\" class=\"center\">" +
                "              <tr>" +
                "                      <th colspan=\"2\"><font style=\"font-size:1.2em;color:black;\">Bill Selection - Cash Breakdown</font></th>" +
                "               </tr>" +
                "                 <tr>" +
                "                 <td> 10 000 x " + currentAccountTransaction.getTenThousand() + " = <b>"  + BVMicroUtils.formatCurrency( 10000 * currentAccountTransaction.getTenThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 5 000 x " + currentAccountTransaction.getFiveThousand() + " = <b>" + BVMicroUtils.formatCurrency(  5000 * currentAccountTransaction.getFiveThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 2 000 x " + currentAccountTransaction.getTwoThousand() + " = <b>" + BVMicroUtils.formatCurrency( 2000 * currentAccountTransaction.getTwoThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 1 000 x" + currentAccountTransaction.getOneThousand() + " = <b>" + BVMicroUtils.formatCurrency(  1000 * currentAccountTransaction.getOneThousand(),rt.getCountryCode()) + "</b><br/>" +
                "                 500 x " + currentAccountTransaction.getFiveHundred() + " = <b>" + BVMicroUtils.formatCurrency( 500 * currentAccountTransaction.getFiveHundred(),rt.getCountryCode()) + "</b><br/>" +
                "                 100 x " + currentAccountTransaction.getOneHundred() + " = <b>" + BVMicroUtils.formatCurrency( 100 * currentAccountTransaction.getOneHundred(),rt.getCountryCode()) + "</b><br/>" +

                "</td><td>"+
                "                 50 x " + currentAccountTransaction.getFifty() + " = <b>" + 50 * currentAccountTransaction.getFifty() + "</b><br/>"  +
                "                 25 x " + currentAccountTransaction.getTwentyFive() + " = <b>" + 25 * currentAccountTransaction.getTwentyFive() + "</b><br/>" +
                "                 10 x " + currentAccountTransaction.getTen() + " = <b>" + 10 * currentAccountTransaction.getTen() + "</b><br/>" +
                "                 5 x " + currentAccountTransaction.getFive() + " = <b>" + 5 * currentAccountTransaction.getFive() + "</b><br/>" +
                "                 1 x " + currentAccountTransaction.getOne() + " = <b>" + 1 * currentAccountTransaction.getOne() + "</b><br/>" +

                "                  </td></tr>" +
                "                 </table></td></tr></table>" +
                "       <table><tr><td><br/><br/>Cashier Signature: ------------------------------------- Customer Signature: -------------------------------------<br/> "+branchRepository.findByCodeAndOrgId(currentAccountTransaction.getBranchCode(),currentAccountTransaction.getOrgId()).getName() +"</td>" +
                "</tr></table>";
        currentBilanzNoInterest = "<html><head></head><body>" + currentBilanzNoInterest  +
                "--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- <br/><br/>"+ currentBilanzNoInterest+"</body></html>";
        return currentBilanzNoInterest;
    }

    public String generateLoanTransactionReceiptPDF(LoanAccountTransaction loanAccountTransaction, RuntimeSetting rt, boolean displayBalance) {

        String showAmount = "";
        String representativeText = StringUtils.equals(loanAccountTransaction.getRepresentative(),BVMicroUtils.getFullName(loanAccountTransaction.getLoanAccount().getUser()))?"":"Remittance: "+loanAccountTransaction.getRepresentative();
        User aUser = userRepository.findByUserName(loanAccountTransaction.getCreatedBy());
        if (loanAccountTransaction.getAccountOwner() != null && StringUtils.equals("true", loanAccountTransaction.getAccountOwner()) && displayBalance) {
            showAmount = BVMicroUtils.formatCurrency(loanAccountTransaction.getAccountBalance(),rt.getCountryCode());
        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String loanBilanzNoInterest = "<font style=\"font-size:1.4em;color:black;\">" +
                "<b>RECEIPT FOR "+loanAccountTransaction.getLoanAccount().getAccountType().getDisplayName()+" TRANSACTION</b></font>" +
                "<table border=\"1\" width=\"100%\">" +
                "<tr> <td><table><tr><td>" +
                "<img width=\"75\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/> Reference No:<br/>"+ loanAccountTransaction.getReference() +
                "</td><td><b><font style=\"font-size:1.6em;color:black;\"> "+ rt.getBusinessName() +"</font></b><br/>" + rt.getAddress()+"<br/>" +rt.getTelephone() +"<br/>" +rt.getEmail() +"<br/>" +
                "</td></tr></table></td>" +
                "<td>"+
                " Branch No: "+loanAccountTransaction.getBranchCode()+
                "<br/>"+loanAccountTransaction.getModeOfPayment()+":" + BVMicroUtils.formatCurrency(loanAccountTransaction.getLoanAmount(),rt.getCountryCode()) +"<br/> "+ representativeText +"<br/>Date:" + BVMicroUtils.formatDateTime(loanAccountTransaction.getCreatedDate()) + "</td></tr>" +
                "<tr><td>" +
                "Account Number:<b>" + BVMicroUtils.getFormatAccountNumber(loanAccountTransaction.getLoanAccount().getAccountNumber())

                + "</b><br/>Customer: <b>"+ BVMicroUtils.getFullName(loanAccountTransaction.getLoanAccount().getUser())+

                "</b> </td>" +
                "<td>Loan Balance: <b>" + showAmount + "</b><br/> Total Repayment Amount: <font style=\"font-size:1.6em;color:black;\">"
                + BVMicroUtils.formatCurrency(loanAccountTransaction.getAmountReceived(),rt.getCountryCode()) + "</font></td></tr>" +
                "        <tr><td colspan=\"2\">" +
                "Agent Representative: <b>"+ BVMicroUtils.getFullName(aUser) +"</b><br/>Notes:"+loanAccountTransaction.getNotes()+"</td>\n" +
                "    </tr></table>" +
                "    <table  border=\"1\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th><font style=\"font-size:1.2em;color:black;\">Bill Selection - Cash Breakdown</font><font style=\"font-size:1.6em;color:black;\"> " + BVMicroUtils.formatCurrency(loanAccountTransaction.getAmountReceived(),rt.getCountryCode()) +" "+ rt.getCurrency()+"</font></th>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "               <td> 10 000 x " + loanAccountTransaction.getTenThousand() + " = <b>" + 10000 * loanAccountTransaction.getTenThousand() + "</b>," +
                "               5 000 x " + loanAccountTransaction.getFiveThousand() + " = <b>" + 5000 * loanAccountTransaction.getFiveThousand() + "</b>," +
                "               2 000 x " + loanAccountTransaction.getTwoThousand() + " = <b>" + 2000 * loanAccountTransaction.getTwoThousand() + "</b>," +
                "               1 000 x " + loanAccountTransaction.getOneThousand() + " = <b>" + 1000 * loanAccountTransaction.getOneThousand() + "</b>" +
                "               500 x " + loanAccountTransaction.getFiveHundred() + " = <b>" + 500 * loanAccountTransaction.getFiveHundred() + "</b>," +
                "               100 x " + loanAccountTransaction.getOneHundred() + " = <b>" + 100 * loanAccountTransaction.getOneHundred() + "</b>," +
                "               50 x " + loanAccountTransaction.getFifty() + " = <b>" + 50 * loanAccountTransaction.getFifty() + "</b>," +
                "               25 x " + loanAccountTransaction.getTwentyFive() + " = <b>" + 25 * loanAccountTransaction.getTwentyFive() + "</b>" +
                "               Amount in Letters: <font color=\"" + rt.getThemeColor() + "\" size=\"8px\"> "
                +               loanAccountTransaction.getLoanAmountInLetters() + "</font> </td></tr>" +
                "        </table>" +
                "       <table><tr><td><br/><br/>Cashier Signature: ------------------------------ Customer Signature: ------------------------------<br/> "+branchRepository.findByCodeAndOrgId(loanAccountTransaction.getBranchCode(),loanAccountTransaction.getOrgId()).getName() +"</td>" +
                "</tr></table><br/>";
        loanBilanzNoInterest = "<html><head></head><body>" + loanBilanzNoInterest + loanBilanzNoInterest+"</body></html>";
        return loanBilanzNoInterest;
    }


    public String generatePDFSavingBilanzList(SavingBilanzList savingBilanzList, SavingAccount savingAccount, String logoPath, RuntimeSetting rt) throws IOException {
        String accountDue = "";
        double dueAmount = savingAccount.getAccountMinBalance() - savingAccount.getAccountBalance();
        if( dueAmount > 0 ){
            accountDue = "Account Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) + " " +rt.getCurrency()+"<br/>";
        }

        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String savingBilanzNoInterest = "<html><head><style>\n" +
                "#transactions {\n" +
                "  border-collapse: collapse;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                "#transactions td, #customers th {\n" +
                "  border: 1px solid #ddd;\n" +
                "  padding: 4px;\n" +
                "}\n" +
                "\n" +
                "#transactions tr:nth-child(even){background-color: \""+rt.getThemeColor2()+"\";}\n" +
                "\n" +
                "#transactions tr:hover {background-color: #ddd;}\n" +
                "\n" +
                "#transactions th {\n" +
                "  padding-top: 6px;\n" +
                "  padding-bottom: 6px;\n" +
                "  text-align: left;\n" +
                "  background-color: #cda893;\n" +
                "  color: white;\n" +
                "}\n" +
                "</style>" +
                "</head><body><br/><br/>" +
                "    <table border=\"0\" width=\"100%\">" +
                "        <tr><td align=\"center\"> <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/><b><font size=\"6\">"+rt.getBusinessName()+"</font></b><br/><font size=\"2\"> "+rt.getSlogan()+"</font></td>" +
                "       <td colspan=\"2\"><b><font size=\"4\" color=\""+rt.getThemeColor()+"\">"+savingAccount.getAccountSavingType().getDisplayName() +" STATEMENT</font></b></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period From:</font></td>" +
                "       <td align=\"right\"><font size=\"4\"> "+BVMicroUtils.formatDate(savingAccount.getCreatedDate())+" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period To:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Account Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getAccountNumber() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Product Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getProductCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Code:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getBranchCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Name:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">" + savingAccount.getUser().getBranch().getName() +" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"4\" color=\""+rt.getThemeColor()+"\">Customer:</font></td>" +
                "       <td align=\"right\">"+ savingAccount.getUser().getGender() +". " + savingAccount.getUser().getFirstName() + " "+savingAccount.getUser().getLastName() +"</td></tr>" +
                "       </table><br/><br/><br/>" +
                "    <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th>Date</th>\n" +
                "                <th style=\"font:12px\"> Branch/MOP </th>\n" +
                "                <th>Agent</th>\n" +
                "                <th>Reference</th>\n" +
                "                <th>Notes</th>\n" +
                "                <th>Debit</th>\n" +
                "                <th>Credit</th>\n" +
                "                <th></th>\n" +
                "            </tr>\n" + getTableList(savingBilanzList,rt) +
                "            <tr>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td colspan=\"3\">Total:<font size=\"10px\"><b>" +savingBilanzList.getTotalSaving()+"</b> " +rt.getCurrency()+"</font></td>\n" +
                "                \n" +
                "            </tr>"+
                "        </table><br/>" +
                "       <table><tr><th>Closing Balance:</th><th> " +savingBilanzList.getTotalSaving()+ " " +rt.getCurrency()+" <br/> "+accountDue+"</th></tr>" +
                "       </table>" +
                "</body></html>";
        return savingBilanzNoInterest;
    }





    public String generatePDFSavingBilanzListInterval(SavingBilanzList savingBilanzList, SavingAccount savingAccount, String logoPath, RuntimeSetting rt, GLSearchDTO glSearchDTO) throws IOException {
        Integer startYear = Integer.parseInt(glSearchDTO.getStartDate().substring(0,4));
        Integer endYear = Integer.parseInt(glSearchDTO.getEndDate().substring(0,4));
        Integer startMonth = Integer.parseInt(glSearchDTO.getStartDate().substring(5,7));
        String startDayofMonth = glSearchDTO.getStartDate().substring(8,10);
        Integer endMonth = Integer.parseInt(glSearchDTO.getEndDate().substring(5,7));
        Integer endDayofMonth = Integer.parseInt(glSearchDTO.getEndDate().substring(8,10));
        LocalDateTime startDate = LocalDateTime.of(startYear,startMonth,5,1,1);
        LocalDateTime endDate = LocalDateTime.of(endYear,endMonth,endDayofMonth,1,1);
        String accountDue = "";
        double dueAmount = savingAccount.getAccountMinBalance() - savingAccount.getAccountBalance();
        if( dueAmount > 0 ){
            accountDue = "Account Due: <b>" + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) + "</b> " +rt.getCurrency()+"<br/>";
        }

        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String savingBilanzNoInterest = "<html><head><style>\n" +
                "#transactions {\n" +
                "  border-collapse: collapse;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                "#transactions td, #customers th {\n" +
                "  border: 1px solid #ddd;\n" +
                "  padding: 4px;\n" +
                "}\n" +
                "\n" +
                "#transactions tr:nth-child(even){background-color: \""+rt.getThemeColor2()+"\";}\n" +
                "\n" +
                "#transactions tr:hover {background-color: #ddd;}\n" +
                "\n" +
                "#transactions th {\n" +
                "  padding-top: 6px;\n" +
                "  padding-bottom: 6px;\n" +
                "  text-align: left;\n" +
                "  background-color: #cda893;\n" +
                "  color: white;\n" +
                "}\n" +
                "</style>" +
                "</head><body><br/><br/>" +
                "    <table border=\"0\" width=\"100%\">" +
                "        <tr><td align=\"center\"> <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/><b><font size=\"6\">"+rt.getBusinessName()+"</font></b><br/><font size=\"2\"> "+rt.getSlogan()+"</font></td>" +
                "       <td colspan=\"2\"><b><font size=\"4\" color=\""+rt.getThemeColor()+"\">"+savingAccount.getAccountSavingType().getDisplayName() +" STATEMENT</font></b></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period From:</font></td>" +
                "       <td align=\"right\"><font size=\"4\"> "+startDayofMonth+" "+startDate.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH)+" "+startYear+" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period To:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ endDayofMonth+" "+endDate.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH)+" "+endYear +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Account Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getAccountNumber() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Product Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getProductCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Code:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getBranchCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Name:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">" + savingAccount.getUser().getBranch().getName() +" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"4\" color=\""+rt.getThemeColor()+"\">Customer:</font></td>" +
                "       <td align=\"right\">"+ savingAccount.getUser().getGender() +". " + savingAccount.getUser().getFirstName() + " "+savingAccount.getUser().getLastName() +"</td></tr>" +
                "       </table><br/><br/><br/>" +
                "       <p>Beginning Balance: <b>"+ savingBilanzList.getSavingBilanzList().get(0).getCurrentBalance()+"</b> " +rt.getCurrency()+"</p>"+"<br/>"+
                "    <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th>Date</th>\n" +
                "                <th style=\"font:12px\"> Branch/MOP </th>\n" +
                "                <th>Agent</th>\n" +
                "                <th>Reference</th>\n" +
                "                <th>Notes</th>\n" +
                "                <th>Debit</th>\n" +
                "                <th>Credit</th>\n" +
                "                <th></th>\n" +
                "            </tr>\n" + getTableList(savingBilanzList,rt) +
                "            <tr>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td colspan=\"3\">Total: <font size=\"10px\"><b>" +savingBilanzList.getTotalSaving()+"</b> " +rt.getCurrency()+"</font></td>\n" +
                "                \n" +
                "            </tr>"+
                "        </table><br/>" +
                "       <p><span style=\"margin-right: 58px;\">Closing Balance: <b>" +savingBilanzList.getTotalSaving()+ "</b> " +rt.getCurrency()+"</span> "+accountDue+"</p>" +
                "      " + "<p>Ending Balance: <b>"+savingBilanzList.getSavingBilanzList().get(savingBilanzList.getSavingBilanzList().size()-1).getCurrentBalance()+"</b> "+rt.getCurrency()+"</p>"+
                "</body></html>";
        return savingBilanzNoInterest;
    }





    public String generatePDFDailySavingBilanzList(SavingBilanzList savingBilanzList, DailySavingAccount savingAccount, String logoPath, RuntimeSetting rt) throws IOException {
        String accountDue = "";
        double dueAmount = savingAccount.getAccountMinBalance() - savingAccount.getAccountBalance();
        if( dueAmount > 0 ){
            accountDue = "Account Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) + " " +rt.getCurrency()+"<br/>";
        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String savingBilanzNoInterest = "<html><head><style>\n" +
                "#transactions {\n" +
                "  border-collapse: collapse;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                "#transactions td, #customers th {\n" +
                "  border: 1px solid #ddd;\n" +
                "  padding: 4px;\n" +
                "}\n" +
                "\n" +
                "#transactions tr:nth-child(even){background-color: \""+rt.getThemeColor2()+"\";}\n" +
                "\n" +
                "#transactions tr:hover {background-color: #ddd;}\n" +
                "\n" +
                "#transactions th {\n" +
                "  padding-top: 6px;\n" +
                "  padding-bottom: 6px;\n" +
                "  text-align: left;\n" +
                "  background-color: #cda893;\n" +
                "  color: white;\n" +
                "}\n" +
                "</style>" +
                "</head><body><br/><br/>" +
                "    <table border=\"0\" width=\"100%\">" +
                "        <tr><td align=\"center\"> <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/><b><font size=\"6\">"+rt.getBusinessName()+"</font></b><br/><font size=\"2\"> "+rt.getSlogan()+"</font></td>" +
                "       <td colspan=\"2\"><b><font size=\"4\" color=\""+rt.getThemeColor()+"\">"+savingAccount.getAccountSavingType().getDisplayName() +" STATEMENT</font></b></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period From:</font></td>" +
                "       <td align=\"right\"><font size=\"4\"> "+BVMicroUtils.formatDate(savingAccount.getCreatedDate())+" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period To:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Account Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getAccountNumber() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Product Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getProductCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Code:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ savingAccount.getBranchCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Name:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">" + savingAccount.getUser().getBranch().getName() +" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"4\" color=\""+rt.getThemeColor()+"\">Customer:</font></td>" +
                "       <td align=\"right\">"+ savingAccount.getUser().getGender() +". " + savingAccount.getUser().getFirstName() + " "+savingAccount.getUser().getLastName() +"</td></tr>" +
                "       </table><br/><br/><br/>" +
                "    <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th>Date</th>\n" +
                "                <th style=\"font:12px\"> Branch/MOP </th>\n" +
                "                <th>Agent</th>\n" +
                "                <th>Reference</th>\n" +
                "                <th>Notes</th>\n" +
                "                <th>Debit</th>\n" +
                "                <th>Credit</th>\n" +
                "                <th></th>\n" +
                "            </tr>\n" + getTableList(savingBilanzList, rt) +
                "            <tr>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td colspan=\"3\">Total:<font size=\"10px\"><b>" +savingBilanzList.getTotalSaving()+"</b> " +rt.getCurrency()+"</font></td>\n" +
                "                \n" +
                "            </tr>"+
                "        </table><br/>" +
                "       <table><tr><th>Closing Balance:</th><th> " +savingBilanzList.getTotalSaving()+ "  " +rt.getCurrency()+"<br/>"+accountDue+"</th></tr>" +
                "       </table>" +
                "</body></html>";
        return savingBilanzNoInterest;
    }




    public String generatePDFCurrentBilanzList(CurrentBilanzList currentBilanzList, CurrentAccount currentAccount,
                                               String logoPath, RuntimeSetting rt) throws IOException {

        String accountDue = "";
        double dueAmount = currentAccount.getAccountMinBalance() - currentAccount.getAccountBalance();
        if( dueAmount > 0 ){
            accountDue = "Account Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) +" " +rt.getCurrency()+"<br/>";
        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String currentBilanzNoInterest = "<html>" + 
                "<head>" + 
                "    <style>" +
                "        * { font-family: Arial, Helvetica, sans-serif; }" +
                "        body { margin: 0px; padding: 0px; }" +
                "        table { width: 100%; border: none; }" +
                "        h6 { font-size: 18px; font-weight: bold; margin: 0px; width: 100%; }" +
                "        h2 { font-size: 22px; font-weight: bold; margin: 0px; width: 100%; }" +
                "        h4 { font-size: 18px; font-weight: bold; margin: 0px; width: 100%; }" +
                "        p { font-size: 14px; font-weight: normal; margin: 0px; width: 100%; }" +
                "        .logo-section { padding-bottom: 15px; }" +
                "        .address-section h2 { margin-bottom: 7px; }" +
                "        .statement-blocks { border-top: 2px solid #000000; border-bottom:  2px solid #000000; padding: 10px 0px; }" +
                "        .statement-blocks table tr td { padding-top: 5px; padding-bottom: 5px; }" +
                "        .statement-blocks label { font-size: 12px; color:#999999; margin: 0px; }" +
                "        .statement-blocks p { font-size: 14px; color: #000000; margin: 0px; padding-top: 3px; }" +
                "        .statement-blocks h6 { padding-top: 3px; }" +
                "        .pdf-list { padding-top: 20px; }" +
                "        .pdf-list table, .pdf-list { border: none; border-collapse: collapse; }" +
                "        .pdf-list td, .pdf-list th { padding: 10px 10px; font-size: 12px; font-weight: normal; }" +
                "        .pdf-list th { background: " + rt.getThemeColor() + "; color: #ffffff; }" +
                "        .pdf-list th:first-child { border-radius: 5px 0 0 0; }" +
                "        .pdf-list th:last-child { border-radius: 0 5px 0 0; }" +
                "        .pdf-list td { color: #000000; border-bottom: 1px solid #cdcdcd; }" +
                "        .pdf-list td.total { text-align: right; font-size: 18px; font-weight: bold; }" +
                "        .pdf-list td.total label { font-weight: normal; }" +
                "        .pdf-list td.wordWrap { word-wrap: break-word; }" +
                "    </style>" +
                "</head>"+
                "<body>"+
                "        <table>" +
                "            <tr>" +
                "                <td>" +
                "                    <table>" +
                "                        <tr>" +
                "                            <td class=\"logo-section\" width=\"50%\" align=\"left\" valign=\"top\">" +
                "                                <img width=\"125px\" src=\"" +prefix+rt.getUnionLogo()+ "\"/>" +
                "                            </td>" +
                "                            <td class=\"address-section\" width=\"50%\" align=\"right\" valign=\"top\">" +
                "                                <h2>Current Account Statement</h2>" +
                "                                <p>" + BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) + "</p>" +
                "                            </td>" +
                "                        </tr>" +
                "                    </table>" +
                "                </td>" +
                "            </tr>" +
                "            <tr>" +
                "                <td>" +
                "                    <table>" +
                "                        <tr>" +
                "                            <td align=\"left\"><h4>"+rt.getBusinessName()+ "</h4></td>" +
                "                            <td align=\"right\"><p>"+rt.getSlogan()+"</p></td>" +
                "                        </tr>" +
                "                    </table>" +
                "                </td>" +
                "            </tr>" +
                "            <tr>" +
                "                <td class=\"statement-blocks\">" +
                "                    <table>" +
                "                        <tr>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Account Number:</label>" +
                "                                <p color=\""+rt.getThemeColor()+"\">" + BVMicroUtils.getFormatAccountNumber(currentAccount.getAccountNumber()) + "</p>" +
                "                            </td>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Period From:</label>" +
                "                                <p>" + BVMicroUtils.formatDate(currentAccount.getCreatedDate()) + "</p>" +
                "                            </td>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Period To:</label>" +
                "                                <p>" + BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) + "</p>" +
                "                            </td>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Product Number:</label>" +
                "                                <p>" + currentAccount.getProductCode() + "</p>" +
                "                            </td>" +
                "                        </tr>" +
                "                        <tr>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Branch Code:</label>" +
                "                                <p>" + currentAccount.getBranchCode() + "</p>" +
                "                            </td>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Branch Name:</label>" +
                "                                <p>" +currentAccount.getUser().getBranch().getName()+ "</p>" +
                "                            </td>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Customer:</label>" +
                "                                <p>" + currentAccount.getUser().getGender() +". " + currentAccount.getUser().getFirstName() +" " + currentAccount.getUser().getLastName() + "</p>" +
                "                            </td>" +
                "                            <td valign=\"top\" width=\"25%\">" +
                "                                <label>Closing Balance:</label>" +
                "                                <h6>" +currentBilanzList.getTotalCurrent()+" " +rt.getCurrency()+ "</h6>" +
                "                            </td>" +
                "                        </tr>" +
                "                    </table>" +
                "                </td>" +
                "            </tr>" +
                "            <tr>" +
                "                <td class=\"pdf-list\">" +
                "                    <table>" +
                "                        <tr>" +
                "                            <th align=\"left\">Date</th>" +
                "                            <th align=\"left\">Branch/MOP / Agent</th>" +
                "                            <th align=\"left\">Reference</th>" +
                "                            <th align=\"left\">Notes</th>" +
                "                            <th align=\"right\">Debit</th>" +
                "                            <th align=\"right\">Credit</th>" +
                "                            <th align=\"right\">Balance</th>" +
                "                        </tr>" + getTableList(currentBilanzList, rt) + 
                "                        <tr>" +
                "                            <td colspan=\"4\"></td>" +
                "                            <td class=\"total\" align=\"right\" colspan=\"3\">" +
                "                               <label>Total:</label>" + currentBilanzList.getTotalCurrent() + " " + rt.getCurrency() +
                "                            </td>" +
                "                        </tr>" +
                "                    </table>" +
                "                </td>" +
                "            </tr>" +
                "        </table>" +
                "</body>" +
            "</html>";
        return currentBilanzNoInterest;
    }



    public String generatePDFLoanBilanzList(LoanBilanzList loanBilanzList, LoanAccount loanAccount, RuntimeSetting rt) throws IOException {
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String savingBilanzNoInterest = "<html><head><style>\n" +
                "#transactions {\n" +
                "  border-collapse: collapse;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                "#transactions td, #customers th {\n" +
                "  border: 1px solid #ddd;\n" +
                "  padding: 4px;\n" +
                "}\n" +
                "\n" +
                "#transactions tr:nth-child(even){background-color: "+rt.getThemeColor2()+";}\n" +
                "\n" +
                "#transactions tr:hover {background-color: "+rt.getThemeColor2()+";}\n" +
                "\n" +
                "#transactions th {\n" +
                "  padding-top: 6px;\n" +
                "  padding-bottom: 6px;\n" +
                "  text-align: left;\n" +
                "  background-color: "+rt.getThemeColor()+";\n" +
                "  color: white;\n" +
                "}\n" +
                "</style>" +
                "</head><body><br/>" +
                "    <table border=\"0\" width=\"100%\">" +
                "        <tr><td align=\"center\"> <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/>"+rt.getBusinessName()+" <br/> Together each achieves more</td>" +
                "       <td colspan=\"2\"><b><font size=\"4\" color=\"green\">"+loanAccount.getAccountType().getDisplayName()+" STATEMENT</font></b></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\"green\">Period From:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(loanAccount.getCreatedDate()) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\"green\">Period To:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\"green\">Account Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.getFormatAccountNumber(loanAccount.getAccountNumber()) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\"green\">Product Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ loanAccount.getProductCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\"green\">Branch Code:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ loanAccount.getBranchCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\"green\">Branch Name:</font></td>" +
                "       <td align=\"right\"><font size=\"4\"> "+loanAccount.getUser().getBranch().getName()+" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"4\" color=\"green\">Customer Shortname:</font></td>" +
                "       <td align=\"right\">" + loanAccount.getUser().getLastName() +"</td></tr>" +
                "       </table><br/><br/><br/>" +
                "    <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th>Date</th>\n" +
                "                <th style=\"font:12px\">Branch/MOP</th>\n" +
                "                <th style=\"font:12px\">Agent</th>\n" +
                "                <th style=\"font:12px\">Reference</th>\n" +
                "                <th style=\"font:12px\">Notes</th>\n" +
                "                <th style=\"font:12px\">VAT</th>\n" +
                "                <th style=\"font:12px\">Interest</th>\n" +
                "                <th style=\"font:12px\">Debit</th>\n" +
                "                <th style=\"font:12px\">Credit</th>\n" +
                "                <th style=\"font:12px\">Balance</th>\n" +
                "            </tr>\n" + getTableList(loanBilanzList) +
                "        </table><br/>" +
//                "    <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"center\">\n" +
//                "       <tr><th id=\"transactions\">Opening Balance</th><th>1000</th></tr>" +
//                "       <tr><td>Credit Sum</td> <td></td></tr>" +
//                "       <tr><td>Debit Sum</td> <td></td></tr></table>" +
                "       <table><tr><th>Closing Balance: </th><th> " +loanBilanzList.getCurrentLoanBalance()+" " +rt.getCurrency()+"</th></tr>" +
//              "       <tr><td>Bamenda Branch, N W Region</td><td>"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</td></tr>" +
                "       </table></body></html>";
        return savingBilanzNoInterest;
    }

    public String generatePDFCurrentBilanzListInterval(CurrentBilanzList currentBilanzList, CurrentAccount currentAccount,
                                                       String logoPath, RuntimeSetting rt, GLSearchDTO glSearchDTO) throws IOException {
        Integer startYear = Integer.parseInt(glSearchDTO.getStartDate().substring(0,4));
        Integer endYear = Integer.parseInt(glSearchDTO.getEndDate().substring(0,4));
        Integer startMonth = Integer.parseInt(glSearchDTO.getStartDate().substring(5,7));
        String startDayofMonth = glSearchDTO.getStartDate().substring(8,10);
        Integer endMonth = Integer.parseInt(glSearchDTO.getEndDate().substring(5,7));
        Integer endDayofMonth = Integer.parseInt(glSearchDTO.getEndDate().substring(8,10));
        LocalDateTime startDate = LocalDateTime.of(startYear,startMonth,5,1,1);
        LocalDateTime endDate = LocalDateTime.of(endYear,endMonth,endDayofMonth,1,1);
        String accountDue = "";
        double dueAmount = currentAccount.getAccountMinBalance() - currentAccount.getAccountBalance();
        if( dueAmount > 0 ){
            accountDue = "Account Due: " + BVMicroUtils.formatCurrency(dueAmount,rt.getCountryCode()) +" " +rt.getCurrency()+"<br/>";
        }
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        String currentBilanzNoInterest = "<html><head><style>\n" +
                "#transactions {\n" +
                "  border-collapse: collapse;\n" +
                "  width: 100%;\n" +
                "}\n" +
                "\n" +
                "#transactions td, #customers th {\n" +
                "  border: 1px solid #ddd;\n" +
                "  padding: 4px;\n" +
                "}\n" +
                "\n" +
                "#transactions tr:nth-child(even){background-color: \""+rt.getThemeColor2()+"\";}\n" +
                "\n" +
                "#transactions tr:hover {background-color: #ddd;}\n" +
                "\n" +
                "#transactions th {\n" +
                "  padding-top: 6px;\n" +
                "  padding-bottom: 6px;\n" +
                "  text-align: left;\n" +
                "  background-color: #cda893;\n" +
                "  color: white;\n" +
                "}\n" +
                "</style>" +
                "</head><body>" +
                "    <table border=\"0\" width=\"100%\">" +
                "        <tr><td align=\"center\"> <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/><br/>"+rt.getBusinessName()+" <br/>"+rt.getSlogan()+"</td>" +
                "       <td colspan=\"2\"><b><font size=\"4\" color=\""+rt.getThemeColor()+"\">CURRENT ACCOUNT STATEMENT</font></b></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.formatDate(new Date(System.currentTimeMillis())) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period From:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ startDayofMonth+" "+startDate.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH)+" "+startYear+"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Period To:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ endDayofMonth+" "+endDate.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH)+" "+endYear +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Account Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ BVMicroUtils.getFormatAccountNumber(currentAccount.getAccountNumber()) +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Product Number:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ currentAccount.getProductCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Code:</font></td>" +
                "       <td align=\"right\"><font size=\"4\">"+ currentAccount.getBranchCode() +"</font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"6\" color=\""+rt.getThemeColor()+"\">Branch Name:</font></td>" +
                "       <td align=\"right\"><font size=\"4\"> "+currentAccount.getUser().getBranch().getName()+" </font></td></tr>" +
                "        <tr><td> </td><td> </td>" +
                "       <td align=\"right\"><font size=\"4\" color=\""+rt.getThemeColor()+"\">Customer:</font></td>" +
                "       <td align=\"right\">"+ currentAccount.getUser().getGender() +". " + currentAccount.getUser().getFirstName() +" " + currentAccount.getUser().getLastName() +"</td></tr>" +
                "       </table><br/>" +"<p>Beginning Balance: <b>"+currentBilanzList.getCurrentBilanzList().get(0).getCurrentBalance()+"</b> "+rt.getCurrency()+"</p>"+"<br/>"+
                "    <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"center\">\n" +
                "            <tr>\n" +
                "                <th style=\"font:12px\">Date</th>\n" +
                "                <th style=\"font:12px\">Branch/MOP </th>\n" +
                "                <th style=\"font:12px\">Agent</th>\n" +
                "                <th style=\"font:12px\">Reference</th>\n" +
                "                <th style=\"font:12px\">Notes</th>\n" +
                "                <th style=\"font:12px\">Debit</th>\n" +
                "                <th style=\"font:12px\">Credit</th>\n" +
                "                <th></th>\n" +
                "            </tr>\n" + getTableList(currentBilanzList, rt) +
                "            <tr>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td></td>\n" +
                "                <td colspan=\"3\">Total: <font size=\"10px\"><b>" +currentBilanzList.getTotalCurrent()+"</b> " +rt.getCurrency()+"</font></td>\n" +
                "                \n" +
                "            </tr>"+
                "        </table><br/>" +
                "       <table><tr><th>Closing Balance:</th><th> " +currentBilanzList.getTotalCurrent()+" " +rt.getCurrency()+"<br/>"+accountDue+"</th></tr>" +
                "       </table></body></html>";
        return currentBilanzNoInterest;
    }

    private String getShortOfMinTable(List<SavingAccount> savingAccounts, RuntimeSetting rt) {
        String tableHtml = "";
        for (SavingAccount savingAccount: savingAccounts){
            tableHtml = tableHtml +
                    "<tr><td>"+BVMicroUtils.getFullName(savingAccount.getUser())+"<br/><i>Tel:"+savingAccount.getUser().getTelephone1()+"</i></td>" +
                    "<td align=\"right\">"+savingAccount.getAccountType().getDisplayName()+"</td>" +
                    "<td align=\"right\">"+BVMicroUtils.formatCurrency(savingAccount.getAccountMinBalance(), rt.getCountryCode())+"</td>" +
                    "<td align=\"right\">"+BVMicroUtils.formatCurrency(savingAccount.getAccountBalance(), rt.getCountryCode())+"</td>" +
                    "<td align=\"right\">"+BVMicroUtils.formatCurrency(savingAccount.getAccountMinBalance()-savingAccount.getAccountBalance(), rt.getCountryCode())+"</td>" +
                    "</tr>";
        }
        return tableHtml;
    }

    private String getTableList(Invoice invoice, RuntimeSetting rt) {
        String tableHtml = "";
        for (InvoiceLineItemDetail invoiceLineItem: invoice.getInvoiceLineItemDetail()){
            tableHtml = tableHtml +
                    "<tr><td>"+invoiceLineItem.getDescription()+"</td>" +
                    "<td align=\"right\">"+invoiceLineItem.getQuantity()+"</td>" +
                    "<td align=\"right\">"+BVMicroUtils.formatCurrency(invoiceLineItem.getUnitPrice(),rt.getCountryCode())+"</td>" +
                    "<td align=\"right\">"+BVMicroUtils.formatCurrency(invoiceLineItem.getTotal(),rt.getCountryCode())+"</td>" +
                    "</tr>";
        }
        return tableHtml;
    }

    private String getTableList(LoanBilanzList loanBilanzList) {
        String tableHtml = "";
        for (LoanBilanz bilanz: loanBilanzList.getLoanBilanzList()){
            tableHtml = tableHtml +  "<tr><td style=\"font:10px\">"+bilanz.getCreatedDate()+"</td>" +
                    "<td>"+bilanz.getBranch()+"/"+bilanz.getModeOfPayment()+"</td>" +
                    "<td>"+bilanz.getAgent()+"</td>" +
                    "<td style=\"font:10px\">"+bilanz.getReference()+"</td>" +
                    "<td style=\"font:10px\">"+bilanz.getNotes()+"</td>" +
                    "<td>"+bilanz.getVatPercent()+"</td>" +
                    "<td>"+bilanz.getInterestAccrued()+"</td>" +
                    "<td>" + getLoanDebitBalance(bilanz)+"</td>" +
                    "<td>" + getLoanCreditBalance(bilanz)+"</td>" +
                    "<td>"+bilanz.getCurrentBalance()+"</td>" +
                    "</tr>";
        }
        return tableHtml;
    }

    private String getLoanDebitBalance(LoanBilanz loanBilanz){
        if(loanBilanz.getModeOfPayment().equals("RECEIPT")){
            return loanBilanz.getLoanAmount();
        }
        return "";
    }

    private String getLoanCreditBalance(LoanBilanz loanBilanz){
        if(!loanBilanz.getModeOfPayment().equals("RECEIPT")){
            return loanBilanz.getAmountReceived();
        }
        return "";
    }

    private double getSavingDebitBalance(SavingBilanz savingBilanz){
        if( savingBilanz.getSavingAmount() < 0){
            return savingBilanz.getSavingAmount();
        }
        return 0;
    }

    private double getSavingCreditBalance(SavingBilanz savingBilanz){
        if(savingBilanz.getSavingAmount() > 0){
            return savingBilanz.getSavingAmount();
        }
        return 0;
    }

    private String getTableList(CurrentBilanzList currentBilanzList, RuntimeSetting rt) {
        String tableHtml = "";
        for (CurrentBilanz bilanz: currentBilanzList.getCurrentBilanzList()){
            String debitValue = "";
            String creditValue = "";
            if(new Double(bilanz.getCurrentAmount())>0){
                creditValue = BVMicroUtils.formatCurrency(bilanz.getCurrentAmount(),rt.getCountryCode());
            }else if(new Double(bilanz.getCurrentAmount())<0){
                debitValue = BVMicroUtils.formatCurrency(bilanz.getCurrentAmount()*-1, rt.getCountryCode());
            }
            tableHtml = tableHtml +  "<tr><td>"+bilanz.getCreatedDate()+"</td>" +
                    "<td>"+bilanz.getBranch()+"/" +bilanz.getModeOfPayment()+"<br/>" + bilanz.getAgent() + "</td>" +
                    "<td>"+bilanz.getReference()+"</td>" +
                    "<td>"+bilanz.getNotes()+"</td>" +
                    "<td align=\"right\">" + debitValue+"</td>" +
                    "<td align=\"right\">" + creditValue +"</td>" +
                    "<td align=\"right\">"+bilanz.getCurrentBalance()+"</td>" +
                    "</tr>";
        }
        return tableHtml;
    }

    private String getTableList(SavingBilanzList savingBilanzList, RuntimeSetting rt) {
        String tableHtml = "";
        for (SavingBilanz bilanz: savingBilanzList.getSavingBilanzList()){
            tableHtml = tableHtml +  "<tr><td style=\"font:12px\">"+bilanz.getCreatedDate()+"</td>" +
                    "<td style=\"font:12px\">"+bilanz.getBranch()+"/" +bilanz.getModeOfPayment()+"</td>" +
                    "<td style=\"font:12px\">"+bilanz.getAgent()+"</td>" +
                    "<td style=\"font:12px\">"+bilanz.getReference()+"</td>" +
                    "<td style=\"font:12px\">"+bilanz.getNotes()+"</td>" +
                    "<td>" + BVMicroUtils.formatCurrency(getSavingDebitBalance(bilanz),rt.getCountryCode())+"</td>" +
                    "<td>" + BVMicroUtils.formatCurrency(getSavingCreditBalance(bilanz),rt.getCountryCode())+"</td>" +
                    "<td>"+bilanz.getCurrentBalance()+"</td>" +
                    "</tr>";
        }
        return tableHtml;
    }

    public ByteArrayOutputStream generatePDF(String completeHtml, HttpServletResponse response)
    {
        ByteArrayOutputStream os = null;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            completeHtml = completeHtml.replaceAll("&","&amp;");
            org.w3c.dom.Document doc = builder.parse(new ByteArrayInputStream(completeHtml.getBytes(StandardCharsets.UTF_8)));
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(doc,null);
            renderer.layout();
            os = new ByteArrayOutputStream();
            renderer.createPDF(os,true);
            os.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return os;
    }

    public String generateAmortizationPDF(Amortization amortizationHT, Amortization amortization, RuntimeSetting rt, User aUser) {
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        return "<html><body>"+
                "<table border=\"0\" width=\"100%\"><tr><td><img width=\"80px\" src=\""+prefix+rt.getUnionLogo()+"\"/></td><td><h3>LOAN PAYMENT DETAILS - AMORTIZATION REPORT</h3></td></tr><tr><td colspan=\"2\">Customer Name: <b>"+amortization.getCustomer() +"</b><br/>Telephone: <b>"+amortization.getTelephone() +"</b></td></tr></table>"+
                "<table border=\"0\" width=\"100%\"><tr>" +

                "<td colspan=\"2\">Number: <br/><b> "+amortization.getLoanMonths()+"</b><br/>"+
                "Start Date:<br/><b> "+amortization.getStartDate()+"</b></td>"+
                "<td>Annual Rate HT: <br/><b> "+amortizationHT.getInterestRate()+
                "</b><br/>Annual Rate TTC: <br/><b> "+amortization.getInterestRate()+"</b></td>"+
                "<td>VAT Interest: <br/><b>"+rt.getVatPercent()+"</b><br/>"+
                "Total VAT Interest: <br/><b>"+BVMicroUtils.formatCurrency(amortization.getInterestHT(),rt.getCountryCode())+"</b></td>"+
                "<td>Monthly Payment:<br/><b>"+amortization.getMonthlyPayment()+"</b><br/> Total Payments:<b><br/>"+amortization.getTotalInterestLoanAmount()+"</b></td>"+
                "<td>Total Interest:<br/>" +
                " <b>"+BVMicroUtils.formatCurrency(amortization.getTotalInterest(),rt.getCountryCode())+"</b><br/>Loan Amount: <br/><b>"+BVMicroUtils.formatCurrency(amortization.getLoanAmount(),rt.getCountryCode())+"</b></td>"+
                "</tr></table>" +
                "<table style= \"{tr:nth-child(even) = background-color: #c2ddf2;}\" width=\"100%\" border=\"1\"><tr><td><br/><b>Number</b></td><td><br/><b>Balance</b></td><td><br/><b>Principal</b></td><td><br/>" +
                "<b>Interest On TTC</b></td><td><br/><b>VAT On Interest</b></td><td><br/><b>Interest On HT</b></td>"+
                "<td><br/><b>Payment</b></td><td><br/><b>Due Date</b></td></tr>"+
                getAmortizationRow(amortization.getAmortizationRowEntryList(),rt)+
                "<tr><td colspan=\"8\" align=\"center\"><br/> Prepared by "+ BVMicroUtils.getFullName(aUser) +" <br/> Date: "+ BVMicroUtils.formatDate(new Date()) +"</td></tr>"+
                "<tr><td colspan=\"8\" align=\"center\"><br/> This loan offer is valid till "+ BVMicroUtils.formatDateTime(LocalDateTime.now().plusDays(14) )+"</td></tr>"+
                "</table></body></html>";
    }

    private String getAmortizationRow(List<AmortizationRowEntry> amortizationRowEntryList, RuntimeSetting rt) {
        String row = "";
        for (AmortizationRowEntry amortizationRowEntry: amortizationRowEntryList){
            row = row +
                    "<tr>" +
                    "<td>"+amortizationRowEntry.getMonthNumber()+"</td>" +
                    "<td>"+amortizationRowEntry.getLoanBalance()+"</td>" +
                    "<td>"+amortizationRowEntry.getPrincipal()+"</td>" +
                    "<td>"+BVMicroUtils.formatCurrency(amortizationRowEntry.getMonthlyInterest(),rt.getCountryCode())+"</td>" +
                    "<td>"+BVMicroUtils.formatCurrency(amortizationRowEntry.getVATOnInterest(),rt.getCountryCode())+"</td>" +
                    "<td>"+BVMicroUtils.formatCurrency(amortizationRowEntry.getInterestOnHT(),rt.getCountryCode())+"</td>" +
                    "<td>"+amortizationRowEntry.getPayment()+"</td>" +
                    "<td>"+amortizationRowEntry.getDate()+"</td>" +
                    "</tr>";
        }
        return row;
    }

    public String generateShortOfMinimum(List<SavingAccount> savingAccounts, RuntimeSetting rt, User user) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        Double total = 0.0;
        for(SavingAccount savingAccount: savingAccounts){
            total += savingAccount.getAccountMinBalance()-savingAccount.getAccountBalance();
        }
        String shortOfMinHtml = "<html><head><style>" +
                "* { font-family: Arial, Helvetica, sans-serif; }" +
                "table { border:none; }" +
                ".address-section { padding-top:20px; padding-bottom: 20px; }" +
                ".address-section h2 { font-size: 20px; font-weight: bold; margin: 0px; padding:0px; color:#000000; line-height: 30px; }" +
                ".address-section p { font-size: 16px; font-weight: normal; margin: 0px; padding:0px; color: #000000; line-height: 22px; }" +
                ".invoice-title h2 { font-size: 30px; font-weight: bold; margin: 0px; padding:0px; color: #000000; line-height: 24px; }" +
                ".invoice-details { background: " + rt.getThemeColor2() + "; border-bottom:5px solid #333333; padding-top:5px; padding-bottom:5px; padding-left: 10px; padding-right:10px; border-radius: 10px 10px 0 0; -moz-border-radius: 10px 10px 0 0; -webkit-border-radius: 10px 10px 0 0; }" +
                ".created-by { padding-bottom: 10px; }" +
                ".created-by span { font-size: 16px; color:#000000; }" +
                ".invoice-snapshot h4 { line-height:22px; font-size: 18px; font-weight: bold; color: #ffffff; margin: 0px; padding: 0px; }" +
                ".invoice-snapshot h4 label { font-size: 14px; font-weight: normal; color: #ffffff; margin: 0px; padding: 0px; }" +
                ".invoice-item-table { border-collapse: collapse; }" +
                ".invoice-item-table th { border-bottom: 2px solid #333333; padding-top: 10px; padding-bottom:10px; } " +
                ".invoice-item-table td { border-bottom: 2px solid #cdcdcd; padding-top: 10px; padding-bottom: 10px; } " +
                ".invoice-item-table td.last-row { border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom:15px; }" +
                ".invoice-item-table td.invoice-total { color: "+ rt.getThemeColor() +"; border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom: 15px; }" +
                ".invoice-total h4 { font-size: 22px; font-weight: bold; margin:0px; padding:0px; }" +
                "</style>" +
                "</head><body>" +
                "<table border=\"0\" width=\"100%\">" +
                "   <tr>" +
                "       <td valign=\"middle\" align=\"left\">" +
                "           <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/>" +
                "       </td>" +
                "       <td valign=\"middle\" align=\"right\" class=\"address-section\">" +
                "           <h2>" + rt.getBusinessName() + "</h2>" +
                "           <p>" + rt.getSlogan() + "</p>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td valign=\"middle\" align=\"left\" class=\"invoice-title\">" +
                "           <br/>" +
                "       </td>" +
                "       <td valign=\"middle\" align=\"right\" class=\"address-section\">" +
                "           <h2>" + user.getGender()+ ". "+ user.getFirstName() + " " + user.getLastName() + "</h2>" +
                "           <p>"+user.getBranch().getEmail()+"<br/>"+user.getBranch().getStreet()+"<br/>"+user.getBranch().getCity()+", "+user.getBranch().getCountry()+"</p>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td class=\"created-by\" colspan=\"2\" valign=\"middle\" align=\"left\">" +
                "           <span>Users who do not meet the minimum balance requirements.</span>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td class=\"invoice-details\" colspan=\"2\" valign=\"middle\" align=\"left\">" +
                "<table border=\"0\" width=\"100%\">" +
                "   <tr>" +
                "       <td class=\"invoice-snapshot\" align=\"left\">" +
                "           <h4><label>Date:</label><br/>" + dateFormat.format(date) + "</h4>" +
                "       </td>" +
                "       <td class=\"invoice-snapshot\" align=\"left\">" +
                "           <h4><label></label><br/></h4>" +
                "       </td>" +
                "       <td class=\"invoice-snapshot\" align=\"left\">" +
                "           <h4><label></label><br/></h4>" +
                "       </td>" +
                "       <td class=\"invoice-snapshot\" align=\"right\">" +
                "           <h4><label>Branch Name:</label><br/>" + user.getBranch().getName() + "</h4>" +
                "       </td>" +
                "   </tr>" +
                "</table>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td colspan=\"2\" valign=\"middle\" align=\"left\">" +
                "           <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"invoice-item-table\">" +
                "            <tr>" +
                "                <th>Name</th>" +
                "                <th align=\"right\">Display Name</th>" +
                "                <th align=\"right\">Balance Required</th>" +
                "                <th align=\"right\">Account Balance</th>" +
                "                <th align=\"right\">Amount Due </th>" +
                "            </tr>" + getShortOfMinTable(savingAccounts,rt) +
                "            <tr>" +
                "                <td colspan=\"3\" class=\"last-row\"><i>www.balaanz.com - user: "+user.getUserName()+" pwd: "+user.getPassword().substring(0,2)+"**** </i><br/><br/><br/><br/>"+ rt.getInvoiceFooter()+"</td>" +
                "                <td class=\"invoice-total\" colspan=\"3\" align=\"right\">" +
                "                   <h3>Total: "+BVMicroUtils.formatCurrency(total,rt.getCountryCode())+ " "+ rt.getCurrency()+"</h3>" +
                "                </td>" +
                "            </tr>"+
                "           </table>" +
                "       </td>" +
                "   </tr>" +
                "</table>" +
                "</body></html>";
        return shortOfMinHtml;
    }

    public String generateTrialBalance(TrialBalanceBilanz trialBalanceBilanz, RuntimeSetting rt) {

        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        return "<html><head><style>" +
                "* { font-family: Arial, Helvetica, sans-serif; }" +
                "table { border:none; }" +
                ".address-section { padding-top:20px; padding-bottom: 20px; }" +
                ".address-section h2 { font-size: 20px; font-weight: bold; margin: 0px; padding:0px; color:#000000; line-height: 30px; }" +
                ".address-section p { font-size: 16px; font-weight: normal; margin: 0px; padding:0px; color: #000000; line-height: 22px; }" +
                ".address-section span { color:#ffffff }" +
                ".invoice-title h2 { font-size: 24px; font-weight: bold; margin: 0px; padding:0px; color: #ffffff; line-height: 24px; }" +
                ".invoice-details { background: " + rt.getThemeColor2() + "; border-bottom:5px solid #333333; padding-top:5px; padding-bottom:5px; padding-left: 10px; padding-right:10px; border-radius: 10px 10px 0 0; -moz-border-radius: 10px 10px 0 0; -webkit-border-radius: 10px 10px 0 0; }" +
                ".created-by { padding-bottom: 10px; }" +
                ".created-by span { font-size: 16px; color:#000000; }" +
                ".invoice-snapshot h4 { line-height:22px; font-size: 18px; font-weight: bold; color: #000000; margin: 0px; padding: 0px; }" +
                ".invoice-snapshot h4 label { font-size: 14px; font-weight: normal; color: #333333; margin: 0px; padding: 0px; }" +
                ".invoice-item-table { border-collapse: collapse; }" +
                ".invoice-item-table th { border-bottom: 2px solid #333333; padding-top: 10px; padding-bottom:10px; } " +
                ".invoice-item-table td { border-bottom: 2px solid #cdcdcd; padding-top: 10px; padding-bottom: 10px; } " +
                ".invoice-item-table td.last-row { border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom:15px; }" +
                ".invoice-item-table tr:last-child td.debit-col { border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom: 15px; }" +
                ".invoice-item-table tr:last-child td.credit-col { border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom: 15px; }" +
                ".debit-col { padding-right: 10px; min-width: 100px; padding-left: 10px;} " +
                ".credit-col { padding-left: 10px; min-width: 100px; } " +
                ".debit-col h4 { font-size: 22px; color: "+ rt.getThemeColor() +"; font-weight: bold; margin:0px; padding:0px; }" +
                ".credit-col h4 { font-size: 22px; color: "+ rt.getThemeColor() +"; font-weight: bold; margin:0px; padding:0px; }" +
                "</style>" +
                "</head><body>" +
                "<table border=\"0\" width=\"100%\">" +
                "   <tr>" +
                "       <td valign=\"middle\" align=\"left\">" +
                "           <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/>" +
                "       </td>" +
                "       <td valign=\"middle\" align=\"right\" class=\"address-section\">" +
                "           <h2>" + rt.getBusinessName() + "</h2>" +
                "           <p>" + rt.getSlogan() + "</p>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td class=\"invoice-details\" colspan=\"2\" valign=\"middle\" align=\"left\">" +
                            "<table border=\"0\" width=\"100%\">" +
                            "   <tr>" +
                            "       <td valign=\"middle\" align=\"left\" class=\"invoice-title\">" +
                            "           <h2>Trial Balance Report</h2>" +
                            "       </td>" +
                            "       <td valign=\"middle\" align=\"right\" class=\"address-section\">" +
                            "           <span>" + trialBalanceBilanz.getStartDate() + " - " + trialBalanceBilanz.getEndDate() + "</span>" +
                            "       </td>" +
                            "   </tr>" +
                            "</table>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td class=\"transaction-container\" colspan=\"2\" valign=\"middle\" align=\"left\">" +
                "           <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"invoice-item-table\">" +
                    "            <tr>" +
                    "                <th valign=\"middle\" align=\"left\">Description</th>" +
                    "                <th valign=\"middle\" class=\"debit-col\" align=\"right\">Debit<br/>" +
                    "                  <h4>" + BVMicroUtils.formatCurrency(trialBalanceBilanz.getCreditTotal(), rt.getCountryCode()) + "</h4> " +
                    "                </th>" +
                    "                <th valign=\"middle\" class=\"credit-col\" align=\"right\">Credit<br/>" +
                    "                  <h4>" + BVMicroUtils.formatCurrency(trialBalanceBilanz.getDebitTotal(),rt.getCountryCode()) + "</h4> " +
                    "                </th>" +
                    "            </tr>" + getTrialBalanceRow(trialBalanceBilanz,rt) +
                    "            <tr>" +
                    "                <td class=\"last-row\"></td>" +
                    "                <td class=\"debit-col\" align=\"right\">" +
                    "                   <h4>" + BVMicroUtils.formatCurrency(trialBalanceBilanz.getCreditTotal(), rt.getCountryCode()) + "</h4>" + 
                    "                </td>" +
                    "                <td class=\"credit-col\" align=\"right\">" +
                    "                   <h4>" + BVMicroUtils.formatCurrency(trialBalanceBilanz.getDebitTotal(),rt.getCountryCode()) + "</h4>" + 
                    "                </td>" +
                    "            </tr>"+
                "           </table>" +
                "       </td>" +
                "   </tr>" +
                "</table>" +
                "</body></html>";

    }


    private String getTrialBalanceRow(TrialBalanceBilanz trialBalanceBilanz, RuntimeSetting rt) {
        String row = "";

        for (TrialBalanceWeb aTrialBalanceWeb: trialBalanceBilanz.getTrialBalanceWeb()){
            double debitTotal = 0;
            double creditTotal = 0;

            if(aTrialBalanceWeb.getCreditBalance().equalsIgnoreCase("false") ){
                if (aTrialBalanceWeb.getTotalDifference() > 0) {
                    debitTotal = aTrialBalanceWeb.getTotalDifference();
                } else if (aTrialBalanceWeb.getTotalDifference() < 0) {
                    debitTotal = aTrialBalanceWeb.getTotalDifference() * -1;
                }
            }
            if(aTrialBalanceWeb.getCreditBalance().equalsIgnoreCase("true") ){
                if (aTrialBalanceWeb.getTotalDifference() < 0) {
                    creditTotal = aTrialBalanceWeb.getTotalDifference() * -1;
                } else if (aTrialBalanceWeb.getTotalDifference() > 0) {
                    creditTotal = aTrialBalanceWeb.getTotalDifference();
                }
            }
            String debit = debitTotal==0.0?"":BVMicroUtils.formatCurrency(debitTotal,rt.getCountryCode());
            String credit = creditTotal==0.0?"":BVMicroUtils.formatCurrency(creditTotal, rt.getCountryCode());
            row = row +
                    "<tr>" +
                    "<td width=\"70%\">"+aTrialBalanceWeb.getName()+"</td>" +
                    "<td class=\"debit-col\" width=\"20%\" align=\"right\">"+debit+"</td>" +
                    "<td class=\"credit-col\" width=\"20%\" align=\"right\">"+credit+"</td>" +
                    "</tr>";
        }
        return row;
    }



    public String generatePDFInvoice(Invoice invoice, RuntimeSetting rt, User customer) throws IOException {
        String prefix = rt.getImagePrefix()==null?"":rt.getImagePrefix();
        User byUserNameAndOrgId = userService.findByUserNameAndOrgId(invoice.getUsername(), invoice.getOrgId());
        String invoiceHtml = "<html><head><style>" +
                "* { font-family: Arial, Helvetica, sans-serif; }" +
                "table { border:none; }" +
                ".address-section { padding-top:20px; padding-bottom: 20px; }" +
                ".address-section h2 { font-size: 20px; font-weight: bold; margin: 0px; padding:0px; color:#000000; line-height: 30px; }" +
                ".address-section p { font-size: 16px; font-weight: normal; margin: 0px; padding:0px; color: #000000; line-height: 22px; }" +
                ".invoice-title h2 { font-size: 30px; font-weight: bold; margin: 0px; padding:0px; color: #000000; line-height: 24px; }" +
                ".invoice-details { background: " + rt.getThemeColor2() + "; border-bottom:5px solid #333333; padding-top:5px; padding-bottom:5px; padding-left: 10px; padding-right:10px; border-radius: 10px 10px 0 0; -moz-border-radius: 10px 10px 0 0; -webkit-border-radius: 10px 10px 0 0; }" +
                ".created-by { padding-bottom: 10px; }" +
                ".created-by span { font-size: 16px; color:#000000; }" +
                ".invoice-snapshot h4 { line-height:22px; font-size: 18px; font-weight: bold; color: #ffffff; margin: 0px; padding: 0px; }" +
                ".invoice-snapshot h4 label { font-size: 14px; font-weight: normal; color: #ffffff; margin: 0px; padding: 0px; }" +
                ".invoice-item-table { border-collapse: collapse; }" +
                ".invoice-item-table th { border-bottom: 2px solid #333333; padding-top: 10px; padding-bottom:10px; } " +
                ".invoice-item-table td { border-bottom: 2px solid #cdcdcd; padding-top: 10px; padding-bottom: 10px; } " +
                ".invoice-item-table td.last-row { border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom:15px; }" +
                ".invoice-item-table td.invoice-total { color: "+ rt.getThemeColor() +"; border-bottom: none; border-top:2px solid #333333; padding-top:15px; padding-bottom: 15px; }" +
                ".invoice-total h4 { font-size: 22px; font-weight: bold; margin:0px; padding:0px; }" +
                "</style>" +
                "</head><body>" +
                "<table border=\"0\" width=\"100%\">" +
                "   <tr>" +
                "       <td valign=\"middle\" align=\"left\">" +
                "           <img width=\"125px\" src=\""+prefix+rt.getUnionLogo()+"\"/>" +
                "       </td>" +
                "       <td valign=\"middle\" align=\"right\" class=\"address-section\">" +
                "           <h2>" + rt.getBusinessName() + "</h2>" +
                "           <p>" + rt.getSlogan() + "</p>" +
                "           <p>Tel: " + rt.getTelephone() + "</p>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td valign=\"middle\" align=\"left\" class=\"invoice-title\">" +
                "           <h2>"+invoice.getInvoiceStatus().name()+"</h2>" +
                "       </td>" +
                "       <td valign=\"middle\" align=\"right\" class=\"address-section\">" +
                "           <h2>" + byUserNameAndOrgId.getGender()+ ". "+ byUserNameAndOrgId.getFirstName() + " " + byUserNameAndOrgId.getLastName() + "</h2>" +
                "           <p>"+byUserNameAndOrgId.getEmail()+"<br/> Tel: "+byUserNameAndOrgId.getTelephone1()+"<br/>"+byUserNameAndOrgId.getAddress()+"<br/>Date: "+BVMicroUtils.formatDateTime(LocalDateTime.now())+"</p>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td class=\"created-by\" colspan=\"2\" valign=\"middle\" align=\"left\">" +
                "           <span>Created By: " + BVMicroUtils.getFullName(customer) + "</span>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td class=\"invoice-details\" colspan=\"2\" valign=\"middle\" align=\"left\">" +
                            "<table border=\"0\" width=\"100%\">" +
                            "   <tr>" +
                            "       <td class=\"invoice-snapshot\" align=\"left\">" +
                            "           <h4><label>"+invoice.getInvoiceStatus().name()+":</label><br/>#" + invoice.getInvoiceNumber() + "</h4>" +
                            "       </td>" +
                            "       <td class=\"invoice-snapshot\" align=\"left\">" +
                            "           <h4><label>Invoice Created:</label><br/>" + BVMicroUtils.formatDate(invoice.getCreatedDate()) + "</h4>" +
                            "       </td>" +
                            "       <td class=\"invoice-snapshot\" align=\"left\">" +
                            "           <h4><label>Due Date:</label><br/>" + invoice.getDueDate().substring(0,10) + "</h4>" +
                            "       </td>" +
                            "       <td class=\"invoice-snapshot\" align=\"right\">" +
                            "           <h4><label>Branch Name:</label><br/>" + invoice.getUser().getBranch().getName() + "</h4>" +
                            "       </td>" +
                            "   </tr>" +
                            "</table>" +
                "       </td>" +
                "   </tr>" +
                "   <tr>" +
                "       <td colspan=\"2\" valign=\"middle\" align=\"left\">" +
                "           <table id=\"transactions\" border=\"0\" width=\"100%\" class=\"invoice-item-table\">" +
                    "            <tr>" +
                    "                <th>Description</th>" +
                    "                <th align=\"right\">Quantity</th>" +
                    "                <th align=\"right\">Unit Price</th>" +
                    "                <th align=\"right\">Amount</th>" +
                    "            </tr>" + getTableList(invoice,rt) +
                    "            <tr>" +
                    "                <td class=\"invoice-total\" colspan=\"4\" align=\"right\">" +
                    "                   <h4>Total: " + BVMicroUtils.formatCurrency(invoice.getTotalSum(),rt.getCountryCode())+" " +rt.getCurrency() + "</h4><br/><br/>"+rt.getInvoiceFooter()+
                "                <br/></td></tr>" +
                "            <tr>" +
                "                <td class=\"last-row\" colspan=\"4\">Make Credit Card Payments here <a href=\"www.balaanz.com/pay/"+rt.getBid()+"\">www.balaanz.com/pay/"+rt.getBid()+"</a></td>" +
                "            </tr>"+
                "           </table>" +
                "       </td>" +
                "   </tr>" +
                "</table>" +
                "</body></html>";
        return invoiceHtml;
    }

}



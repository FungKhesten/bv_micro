package com.bitsvalley.micro.services;

import com.bitsvalley.micro.domain.*;
//import com.bitsvalley.micro.domain.ShoppingCartOrder;
import com.bitsvalley.micro.repositories.OrderItemRepository;
import com.bitsvalley.micro.repositories.POSAccountTransactionRepository;
import com.bitsvalley.micro.repositories.ShopProductRepository;
import com.bitsvalley.micro.repositories.ShoppingCartRepository;
import com.bitsvalley.micro.utils.BVMicroUtils;
import com.bitsvalley.micro.utils.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Fru Chifen
 * 25.02.2023
 */
@Service
public class ShopProductService extends SuperService {

    @Autowired
    private ShopProductRepository shopProductRepository;

    @Autowired
    private GeneralLedgerService generalLedgerService;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private BranchService branchService;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;


    @Autowired
    private POSAccountTransactionRepository posAccountTransactionRepository;

    public Iterable<ShopProduct> findAll(long orgId) {
        Iterable<ShopProduct> all = shopProductRepository.findByOrgId(orgId);
        return all;
    }

    public ShopProduct findByIdAndOrg(long id) {
        ShopProduct shopProduct = shopProductRepository.findById(id).get();
        return shopProduct;
    }

    public ShoppingCart removeProductFromCart(ShopProduct shopProduct, ShoppingCart shoppingCart) {
        OrderItem orderItem = new OrderItem();
        for (OrderItem aOrderItem : shoppingCart.getOrderItems()) {
            if (aOrderItem.getShopProduct().getId() == shopProduct.getId()) {
                if(aOrderItem.getQty() == 1){
                        orderItem = aOrderItem;
                        shoppingCart.setTotal(shoppingCart.getTotal() - aOrderItem.getPrice());
                }else{
                    aOrderItem.setQty(aOrderItem.getQty() - 1);
                    shoppingCart.setTotal(shoppingCart.getTotal() - aOrderItem.getPrice());
                }
                break;
            }

        }
        shoppingCart.getOrderItems().remove(orderItem);

        return shoppingCart;
    }


    public ShoppingCart addProductToCart(ShopProduct shopProduct, ShoppingCart shoppingCart) {
        shoppingCart.getOrderItems();
        OrderItem orderItem = new OrderItem();
        boolean found = false;
        for (OrderItem aOrderItem : shoppingCart.getOrderItems()) {
            if (aOrderItem.getShopProduct().getId() == shopProduct.getId()) {
                aOrderItem.setQty(aOrderItem.getQty() + 1);
                found = true;
                break;
            }
        }
        if(!found){

            orderItem.setShopProduct(shopProduct);
            orderItem.setQty(1);
            orderItem.setName(shopProduct.getName());
            orderItem.setShopProduct(shopProduct);
            orderItem.setPrice(shopProduct.getUnitPrice());
            orderItem.setOrderStatus(OrderStatus.SELECTED);
            shoppingCart.getOrderItems().add(orderItem);

        }
        shoppingCart.setTotal(shoppingCart.getTotal()+shopProduct.getUnitPrice());
        return shoppingCart;
    }

    public void save(ShopProduct shopProduct) {
        shopProductRepository.save(shopProduct);
    }

    public ShoppingCart checkoutShoppingCart(ShoppingCart shoppingCart){

        List<OrderItem> orderItemList = new ArrayList<OrderItem>();
        double sumTotal = 0;
        for(OrderItem aItem: shoppingCart.getOrderItems()){
            aItem.setOrderStatus(OrderStatus.PLACED);
            sumTotal = sumTotal + aItem.getPrice()*aItem.getQty();
            aItem.setPrice(sumTotal);
            orderItemList.add(aItem);
        }
        orderItemRepository.saveAll(orderItemList);
        shoppingCart.setOrderItems(orderItemList);
        shoppingCart.setTotal(sumTotal);
        shoppingCartRepository.save(shoppingCart);
        generalLedgerService.updateGLAfterPosAccountTransaction(createNewPosTransaction(shoppingCart, null));

        return shoppingCart;
    }

    public POSAccountTransaction createNewPosTransaction(ShoppingCart shoppingCart, PosAccount posAccount){
        if( posAccount != null ){
//            PosAccount byAccountNumber = posAccountRepository.findByAccountNumberAndOrgId(accountNumber,orgId);
        }
        POSAccountTransaction posAccountTransaction = new POSAccountTransaction();
        posAccountTransaction.setPosAccount(null);
        posAccountTransaction.setWithdrawalDeposit(1);
        posAccountTransaction.setSumTotal(shoppingCart.getTotal());
        posAccountTransaction.setNotes("POS GL Account to transfer.  ");
        posAccountTransaction.setCreatedBy(getLoggedInUserName());
        posAccountTransaction.setReference(BVMicroUtils.getSaltString());
//        Date date = BVMicroUtils.formatDate(new Date(System.currentTimeMillis()));
        posAccountTransaction.setCreatedDate(LocalDateTime.now());
        posAccountTransaction.setModeOfPayment(BVMicroUtils.CASH);
        Branch branchInfo = branchService.getBranchInfo(getLoggedInUserName());
        posAccountTransaction.setBranch(branchInfo.getId());
        posAccountTransaction.setBranchCode(branchInfo.getCode());
        posAccountTransaction.setBranchCountry(branchInfo.getCountry());
//                    currentAccountTransaction.setAccountOwner(byAccountNumber.getUser().getLastName());
        posAccountTransaction.setOrgId(branchInfo.getOrgId());
        posAccountTransaction.setCurrentAmountInLetters("SYSTEM");
        posAccountTransactionRepository.save(posAccountTransaction);
//        if( byAccountNumber != null){
//            byAccountNumber.getCurrentAccountTransaction().add(posAccountTransaction);
//        }
//        currentAccountRepository.save(byAccountNumber);
//        updateGeneralLedger(posAccountTransaction, BVMicroUtils.POS_GL_3333, BVMicroUtils.CREDIT, posAccountTransaction.getSumTotal(), true);
        return posAccountTransaction;
    }

}

package com.bitsvalley.micro.services;

import com.bitsvalley.micro.domain.AccountType;
import com.bitsvalley.micro.domain.SavingAccount;
import com.bitsvalley.micro.domain.User;
import com.bitsvalley.micro.domain.UserRole;
import com.bitsvalley.micro.repositories.AccountTypeRepository;
import com.bitsvalley.micro.repositories.SavingAccountRepository;
import com.bitsvalley.micro.repositories.UserRepository;
import com.bitsvalley.micro.repositories.UserRoleRepository;
import com.bitsvalley.micro.utils.BVMicroUtils;
import com.bitsvalley.micro.webdomain.SavingReportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReportService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountTypeRepository accountTypeRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    SavingAccountRepository savingAccountRepository;

    public SavingReportDTO savingReports( String loggedInUserName , String countryCode) {

        ArrayList<UserRole> customers = new ArrayList<UserRole>();
        UserRole roleByName = userRoleRepository.findByName(BVMicroUtils.ROLE_CUSTOMER);
        customers.add(roleByName);
        User user = userRepository.findByUserName(loggedInUserName);
        StringBuilder descriptionValues = new StringBuilder("[\"");
        int index = 0;
        StringBuilder dueValues = new StringBuilder("[");
        StringBuilder paidValues = new StringBuilder("[");

        double sumTotalPaid = 0;
        double sumTotalDue = 0;

        for (AccountType accountType: accountTypeRepository.findByOrgIdAndCategoryAndActiveTrue(user.getOrgId(),BVMicroUtils.SAVINGS)){
            double totalDue = 0.0;
            double totalPaid = 0.0;

            List<SavingAccount> savingAccounts = savingAccountRepository.findByOrgIdAndAccountType(user.getOrgId(), accountType);

            if (index > 0){
                descriptionValues.append("\",\"");
            }
            index++;
            descriptionValues.append(accountType.getDisplayName());
            for ( SavingAccount aSavingAccount: savingAccounts ) {

                double accountMinBalance = aSavingAccount.getAccountMinBalance();
                double accountBalance = aSavingAccount.getAccountBalance();

                if(accountBalance < accountMinBalance){
                    totalDue = totalDue + (accountMinBalance - accountBalance);
                    sumTotalDue = sumTotalDue +  (accountMinBalance - accountBalance);
                }
                totalPaid = totalPaid + accountBalance;
                sumTotalPaid = sumTotalPaid + accountBalance;

            }
            dueValues.append(totalDue).append(",");
            paidValues.append(totalPaid).append(",");
        }

        dueValues.deleteCharAt(dueValues.lastIndexOf(","));
        paidValues.deleteCharAt(paidValues.lastIndexOf(","));
        descriptionValues.append("\"],");
        dueValues.append("]");
        paidValues.append("]");

        SavingReportDTO savingReportDTO = new SavingReportDTO();
        savingReportDTO.setDescriptionValues(descriptionValues.toString());
        savingReportDTO.setPaidValues(paidValues.toString());
        savingReportDTO.setDueValues(dueValues.toString());
        savingReportDTO.setSumTotalPaid("\"Paid - " + BVMicroUtils.formatCurrency(sumTotalPaid, countryCode)+"\",");
        savingReportDTO.setSumTotalDue("\"Due - "+ BVMicroUtils.formatCurrency(sumTotalDue, countryCode)+"\",");

//        model.put("descriptionValues","[\"Web1\",\"App1\",\"App2\",\"CSe\"],");
//        model.put("paidValues","[13000,20000,17000,25000]");
//        model.put("dueValues","[33000,40000,35000,50000]");

        return savingReportDTO;
    }

}

package com.bitsvalley.micro.services;

import com.bitsvalley.micro.utils.CollectionRequestStatus;
import com.bitsvalley.micro.utils.SignatureGenerator;
import com.bitsvalley.micro.webdomain.CollectionRequest;
import com.jayway.jsonpath.JsonPath;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.UUID;

@Service
public class CMRService {

    RestTemplate restTemplate = new RestTemplate();
    String USER_NAME = "bitsvalley";
    String PASSWORD = "bit$v@lley2011";

    public CollectionRequestStatus sendMomoCollectionRequest(BigDecimal collectionAmount, Long collectionPhoneNumber) {

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Client", "bitsvalley");
        try {
            requestHeaders.add("Captcha-Token", SignatureGenerator.generateSignature());
        } catch (Exception e) {
            e.printStackTrace();
        }
        requestHeaders.add("Authorization", "Bearer " + getAuthToken());
        HttpEntity<CollectionRequest> requestEntity = new HttpEntity<CollectionRequest>(getDebitRequest(collectionAmount,collectionPhoneNumber), requestHeaders);
        ResponseEntity<CollectionRequestStatus> responseEntity = restTemplate.exchange("https://developer.folelogix.com/mobilepay/client/api/payments/collect", HttpMethod.POST, requestEntity, CollectionRequestStatus.class);
        return responseEntity.getBody();
    }

    public String getAuthToken() {
        String encodedCredentials = Base64.getEncoder().encodeToString((USER_NAME + ":" + PASSWORD).getBytes());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Authorization", "Basic " + encodedCredentials);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange("https://developer.folelogix.com/authentication/api/oauth/token", HttpMethod.POST, requestEntity, String.class);
        String tokenResponse = responseEntity.getBody();
        String authTokenExpression = "$.token";
        String authToken = JsonPath.parse(tokenResponse).read(authTokenExpression, String.class);
        return authToken;
    }

    public CollectionRequest getDebitRequest(BigDecimal collectionAmount, Long collectionPhoneNumber) {
        CollectionRequest debit = new CollectionRequest();
        debit.setTransactionId(UUID.randomUUID().toString());
        debit.setAmount(collectionAmount);
        debit.setCashOutFee(BigDecimal.valueOf(0));
        debit.setTransactionFee(BigDecimal.valueOf(120));
        debit.setCurrency("EUR");
        debit.setPhoneNumber(collectionPhoneNumber);
        debit.setNote("Testing a client collection");
        debit.setMsisdnProvider("MTN");
        return debit;
    }

}

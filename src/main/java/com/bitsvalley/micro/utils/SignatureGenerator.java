package com.bitsvalley.micro.utils;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Base64;


public class SignatureGenerator {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh 'o''clock' a, EEE, d MMM yyyy");

    public static String generateSignature() throws NoSuchAlgorithmException, InvalidKeyException {
        String valueToDigest = getMessageDigest();
        String messageDigest = generateHmacSignature(valueToDigest, getKey());
        return messageDigest;
    }

    private static String generateHmacSignature(String message, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException {
        byte[] bytes = hmac("HmacSHA256", key, message.getBytes());
        return "signature="+Base64.getEncoder().encodeToString(bytes);
    }

    private static byte[] hmac(String algorithm, byte[] key, byte[] message) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(message);
    }

    private static byte[] getKey() {
        return "e1f8ee654dbf4e7bb4a0c235e4f0bed3".getBytes(StandardCharsets.UTF_8);
    }

    private static String getMessageDigest() {
        // Get the current date and time in UTC
        Instant instant = Instant.now();

        // Set the time zone to GMT
        ZoneId zone = ZoneId.of("GMT");

        // Format the date and time as a string
        String dateTime = instant.atZone(zone).format(formatter);
        return dateTime;
    }
}
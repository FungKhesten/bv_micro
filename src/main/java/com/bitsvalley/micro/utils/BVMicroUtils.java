package com.bitsvalley.micro.utils;

import com.bitsvalley.micro.domain.User;
import com.bitsvalley.micro.webdomain.RuntimeSetting;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class BVMicroUtils {


    public static final String TRANSFER = "TRANSFER";

    public static final String CURRENT_ACCOUNT = "CURRENT ACCOUNT";
    public static final String SAVING_ACCOUNT = "SAVING ACCOUNT";
    public static final String DAILY_SAVING_ACCOUNT = "DAILY SAVING ACCOUNT";
    public static final String LOAN_ACCOUNT = "LOAN ACCOUNT";
    public static final String SHARE_ACCOUNT = "SHARE ACCOUNT";
    public static final String APARTMENT_ACCOUNT = "APARTMENT ACCOUNT";
    public static final String NOTES = "NOTES";

    public static final String CURRENT_TO_GL_TRANSFER = "CURRENT_TO_GL_TRANSFER";
    public static final String SAVING_TO_GL_TRANSFER = "SAVING_TO_GL_TRANSFER";

    public static final String CURRENT_LOAN_TRANSFER = "CURRENT_LOAN_TRANSFER";
    public static final String SAVING_SHARE_TRANSFER = "SAVING_SHARE_TRANSFER";
    public static final String DEBIT_DEBIT_TRANSFER = "DEBIT_DEBIT_TRANSFER";
    public static final String DEBIT_CURRENT_TRANSFER = "DEBIT_CURRENT_TRANSFER";

    public static final String CURRENT_DEBIT_TRANSFER = "CURRENT_DEBIT_TRANSFER";
    public static final String CURRENT_SHARE_TRANSFER =  "CURRENT_SHARE_TRANSFER";
    public static final String CURRENT_MOMO_TRANSFER =  "CURRENT_MOMO_TRANSFER";
    public static final String CURRENT_CURRENT_TRANSFER = "CURRENT_CURRENT_TRANSFER";

    public static final String SAVINGS_MINIMUM_BALANCE_ADDED_BY = "Savings minimum balance added by: ";
    public static final String SAVING_ACCOUNT_CREATED = "Saving account created ";
    public static final String DAILY_SAVING_ACCOUNT_CREATED = "Daily Saving account created ";



    public static final String CURRENT_ACCOUNT_CREATED = "Current account created ";

    public static final String CURRENT = "CURRENT";

    public static final String GENERAL_SAVINGS = "GENERAL_SAVINGS";
    public static final String RETIREMENT_SAVINGS = "RETIREMENT_SAVINGS";
    public static final String DAILY_SAVINGS = "DAILY_SAVINGS";
    public static final String MEDICAL_SAVINGS = "MEDICAL_SAVINGS";
    public static final String SOCIAL_SAVINGS = "SOCIAL_SAVINGS";
    public static final String BUSINESS_SAVINGS = "BUSINESS_SAVINGS";
    public static final String CHILDREN_SAVINGS = "CHILDREN_SAVINGS";
    public static final String REAL_ESTATE_SAVINGS = "REAL_ESTATE_SAVINGS";
    public static final String EDUCATION_SAVINGS = "EDUCATION_SAVINGS";

    public static final String CONSUMPTION_LOAN = "CONSUMPTION_LOAN";
    public static final String SHORT_TERM_LOAN = "SHORT_TERM_LOAN";
    public static final String AGRICULTURE_LOAN = "AGRICULTURE_LOAN";
    public static final String BUSINESS_INVESTMENT_LOAN = "BUSINESS_INVESTMENT_LOAN";
    public static final String SCHOOL_FEES_LOAN = "SCHOOL_FEES_LOAN";
    public static final String REAL_ESTATE_LOAN = "REAL_ESTATE_LOAN";
    public static final String OVERDRAFT_LOAN = "OVERDRAFT_LOAN";
    public static final String NJANGI_FINANCING = "NJANGI_FINANCING";
    public static final String NO_NAME = "NO_NAME";

    public static final String ORDINARY_SHARE = "ORDINARY_SHARE";

    public static final String CUSTOMER_IN_USE = "customerInUse";
    public static final String DATE_FORMATTER= "dd-MM-yyyy HH:mm";
    public static final String DATE_ONLY_FORMATTER= "dd-MM-yyyy";
    public static final String DATE_US_ONLY_FORMATTER= "yyyy-MM-dd";
//    public static final String SYSTEM = "SYSTEM";
    public static final String REGULAR_MONTHLY_PAYMENT_MISSING = "Regular Monthly payment not on schedule might be missing payment for some months. " +
            "Please check the account statement";
    public static final String MINIMUM_BALANCE_NOT_MET_FOR_THIS_ACCOUNT = "Minimum Balance not met for this account ";

    public static final String PENDING_APPROVAL = "PENDING_APPROVAL";
    public static final String PENDING_PAYOUT = "PENDING_PAYOUT";
    public static final String ACTIVE = "ACTIVE";
    public static final String INACTIVE = "INACTIVE";

    public static final String LOAN_MUST_BE_IN_ACTIVE_STATE = "SELECTED LOAN MUST BE IN 'ACTIVE' STATE";

    public static final String CREDIT = "CREDIT";
    public static final String DEBIT = "DEBIT";
    public static final String EXPENSES = "EXPENSES";
    public static final String LIABILITIES = "LIABILITIES";
    public static final String REVENUE = "REVENUE";
    public static final String ASSETS = "ASSETS";
    public static final String CASH = "CASH";
    public static final String EVENT = "EVENT";
    public static final String SAVINGS = "SAVINGS";
    public static final String LOAN = "LOAN";
    public static final String VAT = "VAT";
    public static final String LOAN_INTEREST = "LOAN_INTEREST";

//    public static final String CASH_GL_1001 = "CASH GL 1001";
    public static final String CASH_GL_5001 = "CASH_GL_5001";
    public static final String SHARE_GL_XXXX = "SHARE_GL_XXXX";

    public static final String CURRENT_GL_3004 = "CURRENT_GL_3004";
    public static final String SHARE_GL_5004 = "SHARE_GL_5004";

    public static final String GL_4002 = "GL_4002";


    public static final String UNIT_SHARE_PRICE = "unitSharePrice";
    public static final String UNIT_SHARE_PREFERENCE_PRICE = "unitSharePreferencePrice";
    public static final String SHARE = "SHARE";
    public static final String GL_3001 = "GL_3001";
    public static final String INIT_SYSTEM = "INIT_SYSTEM";

    public static final String GL_3004 = "GL_3004";
    public static final String GL_3005 = "GL_3005";
    public static final String GL_3006 = "GL_3006";
    public static final String GL_3007 = "GL_3007";
    public static final String GL_3008 = "GL_3008";
    public static final String GL_3009 = "GL_3009";

    public static final String GL_3010 = "GL_3010";
    public static final String GL_3011 = "GL_3011";
    public static final String GL_3012 = "GL_3012";
    public static final String GL_3013 = "GL_3013";
    public static final String GL_3014 = "GL_3014";
    public static final String GL_3015 = "GL_3015";

    public static final String GL_3016 = "GL_3016";
    public static final String GL_3017 = "GL_3017";
    public static final String GL_3018 = "GL_3018";
    public static final String GL_3019 = "GL_3019";
    public static final String GL_3020 = "GL_3020";
    public static final String GL_3021 = "GL_3021";
    public static final String GL_3022 = "GL_3022";
    public static final String GL_3023 = "GL_3023";
    public static final String GL_5004 = "GL_5004";
//    public static final String GL_3003 = "GL_3003";
    public static final String GL_5001 = "GL_5001";
//    public static final String GL_7001 = "GL_7001";
    public static final String GL_TRANSFER = "GL_TRANSFER";
    public static final String NJANGI_FINANCING_LOAN_INTEREST = "NJANGI_FINANCING_LOAN_INTEREST";
    public static final String OVERDRAFT_LOAN_INTEREST = "OVERDRAFT_LOAN_INTEREST";
    public static final String REAL_ESTATE_LOAN_INTEREST = "REAL_ESTATE_LOAN_INTEREST";
    public static final String SCHOOL_FEES_LOAN_INTEREST = "SCHOOL_FEES_LOAN_INTEREST";
    public static final String BUSINESS_INVESTMENT_LOAN_INTEREST = "BUSINESS_INVESTMENT_LOAN_INTEREST";
    public static final String AGRICULTURE_LOAN_INTEREST = "AGRICULTURE_LOAN_INTEREST";
    public static final String CONSUMPTION_LOAN_INTEREST = "CONSUMPTION_LOAN_INTEREST";
    public static final String SHORT_TERM_LOAN_INTEREST = "SHORT_TERM_LOAN_INTEREST";
    public static final String GL_7015 = "GL_7015";
    public static final String GL_7016 = "GL_7016";
    public static final String GL_7017 = "GL_7017";
    public static final String GL_7018 = "GL_7018";
    public static final String GL_7019 = "GL_7019";
    public static final String GL_7020 = "GL_7020";
    public static final String GL_7021 = "GL_7021";
    public static final String GL_7022 = "GL_7022";

    public static final String ROLE_CASHIER = "ROLE_CASHIER";
    public static final String ROLE_ACCOUNT_BALANCES = "ROLE_ACCOUNT_BALANCES";
    public static final String ROLE_CUSTOMER_TRANSACTIONS_PRINT = "ROLE_CUSTOMER_TRANSACTIONS_PRINT";
    public static final String ROLE_MAIN_SEARCH_USERS ="ROLE_MAIN_SEARCH_USERS";
    public static final String ROLE_GL_ACCOUNT_EXPENSE_ENTRY = "ROLE_GL_ACCOUNT_EXPENSE_ENTRY";
    public static final String ROLE_MAIN_GL_ACCOUNTS = "ROLE_MAIN_GL_ACCOUNTS";
    public static final String ROLE_CREATE_GL_ACCOUNT = "ROLE_CREATE_GL_ACCOUNT";
    public static final String ROLE_CUSTOMER = "ROLE_CUSTOMER";
    public static final String ROLE_DAILY_COLLECTION_CUSTOMER = "ROLE_DAILY_COLLECTION_CUSTOMER";
    public static final String ROLE_MANAGER = "ROLE_MANAGER";
    public static final String ROLE_EMPLOYEE = "ROLE_EMPLOYEE";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String CREDIT_CARD = "CREDIT CARD";

    public static final String THEME_COLOR_2 = "themeColor2";
    public static final String THEME_COLOR = "themeColor";
    public static final String MAKE_A_PAYMENT = "makeAPayment";
    public static final String CURRENCY = "currency";
    public static final String BID = "bid";
    public static final String INVOICE_FOOTER = "invoiceFooter";
    public static final String BILL_SELECTION_ENABLED = "billSelectionEnabled";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String CURRENT_ORG = "current_org";
    public static final String SHOPPING_CART = "shoppingCart";
    public static final String SHOP_PRODUCTS = "shopProducts";
    public static final String SHOP_CATEGORIES = "shopCategories";
    public static final String POS_TO_GL_TRANSFER = "POS_TO_GL_TRANSFER";
    public static final String POS_GL_3333 = "POS_GL_3333";
    public static final String POINT_OF_SALE = "POINT_OF_SALE";
    public static final String SALES = "SALES";
    public static final String CONTEXT_NAME = "CONTEXT_NAME";



    public static  String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; //TODO: avoid collision
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 9) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }


    public  static String formatDateTime(LocalDateTime localDateTime) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
            String formatDateTime = localDateTime.format(formatter);
            return formatDateTime;
        }

    public  static String formatDateOnly(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_ONLY_FORMATTER);
        String formatDateTime = localDate.format(formatter);
        return formatDateTime;
    }

    public  static String formatUSDateOnly(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_US_ONLY_FORMATTER);
        String formatDateTime = localDate.format(formatter);
        return formatDateTime;
    }

    public static String formatCurrency(double totalSaved) {
        return formatCurrency(totalSaved, "us");
    }

    public static String formatCurrency(double totalSaved, String countryCode) {

        String total = "";
        if("us".equalsIgnoreCase(countryCode)) {

            Locale locale = new Locale("en", "US");
            NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
            total = fmt.format(totalSaved);
            return total;
        } else  { //("CM".equalsIgnoreCase(countryCode))

        Locale locale = new Locale("en", "CM");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        total = fmt.format(totalSaved);
        total = total.replace("XAF","");
            total = total.replace("FCFA","");
//        total = total.replaceFirst("F","-");
//        total = total.replaceFirst("A","");
    }
        return total;
    }

    public static String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy hh:mm");
        String format = formatter.format(date);
        return format;
    }


    public static Date formatDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = null;
        try {
            parse = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parse;
    }

    public static LocalDateTime formatLocaleDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = null;
        try {
            parse = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LocalDateTime localDateTime = convertToLocalDateTimeViaMilisecond(parse);
        return localDateTime;
    }


    public static String getCobacSavingsAccountNumber(String countryCode, String productCode,
                                                      int numberOfProductsInBranch, String userAccountNumber,
                                                      String branch) {

        //country code - 3 digits
        //product code - 2 digits
        //number of products for that branch - 5 digits
        //customer number starting with 101 - 11 digit
        //branch code 001 - 3 digit

//        accountNumber = accountNumber.replaceFirst("1011", "101");

        numberOfProductsInBranch = numberOfProductsInBranch + 100000;
        String noOfProductsInBranch = numberOfProductsInBranch + "";
        noOfProductsInBranch = noOfProductsInBranch.replaceFirst("1", "");

        userAccountNumber = countryCode + productCode + noOfProductsInBranch + userAccountNumber + branch;

        return userAccountNumber;
    }


    public static LocalDateTime convertToLocalDateTimeViaMilisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static LocalDate convertToLocalDate(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static Date convertToDate(LocalDateTime dateToConvert) {
        LocalDateTime localDateTime = LocalDateTime.of(
                dateToConvert.getYear(),
                dateToConvert.getMonth(),
                dateToConvert.getDayOfMonth(),
                dateToConvert.getHour(),
                dateToConvert.getMinute(),
                dateToConvert.getSecond());
        Date aDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return aDate;

    }


    public static String getFormatAccountNumber(String accountNumber) {
        return accountNumber.substring(0,5) + " " + accountNumber.substring(5,10) + " " + accountNumber.substring(10,21) + " "+ accountNumber.substring(21,23);
    }


    public static String getOppositeCreditOrDebit(String creditOrDebit) {
        if(creditOrDebit.equals(BVMicroUtils.DEBIT)){
            return BVMicroUtils.CREDIT;
        }
        return BVMicroUtils.DEBIT;
    }

    public static String getFullName(User aUser) {
        aUser.setGender(StringUtils.isEmpty(aUser.getGender())?"":aUser.getGender());
        return aUser.getGender() + " " + aUser.getFirstName() + " " + aUser.getLastName();
    }

//    @ResponseBody
//    public static byte[] getLogoImage(Path path) throws IOException {
////        RuntimeSetting runtimeSetting = (RuntimeSetting)request.getSession().getAttribute("runtimeSettings");
////        Path path = Paths.get(logoPath);
//        byte[] data = Files.readAllBytes(path);
//        return data;
//    }
}

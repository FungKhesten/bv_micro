package com.bitsvalley.micro.controllers;

import com.bitsvalley.micro.domain.*;
import com.bitsvalley.micro.repositories.ProductCategoryRepository;
import com.bitsvalley.micro.services.ProductCategoryService;
import com.bitsvalley.micro.services.ShopProductService;
import com.bitsvalley.micro.services.WarehouseLocationService;
import com.bitsvalley.micro.utils.BVMicroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Fru Chifen
 * 11.06.2021
 */
@Controller
public class POSController extends SuperController {

    @Autowired
    private ShopProductService shopProductService;

    @Autowired
    private ProductCategoryService productCategoryService;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private WarehouseLocationService warehouseLocationService;

    @GetMapping(value = "/posProducts")
    public String posproducts(ModelMap model, HttpServletRequest request) {
        return "posProducts";
    }

    @GetMapping(value = "/posCheckout")
    public String posCheckout(ModelMap model, HttpServletRequest request) {
        return "posCheckout";
    }


    @GetMapping(value = "/posConfirmPayment")
    public String posConfirmPayment(ModelMap model, HttpServletRequest request) {
        return "posConfirmPayment";
    }


    @GetMapping(value = "/showPosCategoryProducts/{id}")
    public String showPosCategoryProducts(@PathVariable("id") Long id, ModelMap model, HttpServletRequest request) {

        long orgID = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        ProductCategory productCategory = productCategoryRepository.findById(id).get();
        Iterable<ShopProduct> shopProducts = productCategoryService.findByOrgIdProductCategory(orgID, productCategory);
        model.put("shoppingCart", (ShoppingCart) request.getSession().getAttribute(BVMicroUtils.SHOPPING_CART));
        model.put("shopProducts", shopProducts);
        model.put("categories", productCategoryService.findAll(orgID));
        return "posCategoryProducts";
    }



    @GetMapping(value = "/removeFromCart/{id}")
    public String removeFromCart(@PathVariable("id") Long id, ModelMap model, HttpServletRequest request) {

        ShoppingCart shoppingCart = (ShoppingCart) request.getSession().getAttribute(BVMicroUtils.SHOPPING_CART);
        long orgID = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        ShopProduct shopProduct = shopProductService.findByIdAndOrg(id);
        shoppingCart = shopProductService.removeProductFromCart(shopProduct, shoppingCart);
//        Iterable<ShopProduct> shopProducts = productCategoryService.findByI(id);
        Iterable<ShopProduct> shopProducts = productCategoryService.findByOrgIdProductCategory(orgID, shopProduct.getProductCategory());

        request.getSession().setAttribute(BVMicroUtils.SHOPPING_CART, shoppingCart);
        model.put("shopProducts", shopProducts);
        model.put("categories", productCategoryService.findAll(orgID));

        //TODO: read from session and avoid many db calls
//        request.getSession().setAttribute(BVMicroUtils.SHOPPING_CART,shoppingCart);
//        request.getSession().setAttribute(BVMicroUtils.SHOP_PRODUCTS,shopProducts);
//        request.getSession().setAttribute(BVMicroUtils.SHOP_CATEGORIES,productCategoryService.findAll(orgID));

        return "posCategoryProducts";
    }


    @GetMapping(value = "/addToCart/{id}")
    public String addToCart(@PathVariable("id") Long id, ModelMap model, HttpServletRequest request) {

        ShoppingCart shoppingCart = (ShoppingCart) request.getSession().getAttribute(BVMicroUtils.SHOPPING_CART);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
        }
        long orgID = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        ShopProduct shopProduct = shopProductService.findByIdAndOrg(id);
        shoppingCart = shopProductService.addProductToCart(shopProduct, shoppingCart);
//        Iterable<ShopProduct> shopProducts = productCategoryService.findByI(id);
        Iterable<ShopProduct> shopProducts = productCategoryService.findByOrgIdProductCategory(orgID, shopProduct.getProductCategory());

        request.getSession().setAttribute(BVMicroUtils.SHOPPING_CART, shoppingCart);
        model.put("shopProducts", shopProducts);
        model.put("categories", productCategoryService.findAll(orgID));

        //TODO: read from session and avoid many db calls
//        request.getSession().setAttribute(BVMicroUtils.SHOPPING_CART,shoppingCart);
//        request.getSession().setAttribute(BVMicroUtils.SHOP_PRODUCTS,shopProducts);
//        request.getSession().setAttribute(BVMicroUtils.SHOP_CATEGORIES,productCategoryService.findAll(orgID));

        return "posCategoryProducts";
    }

    @GetMapping(value = "/posCategoryProducts")
    public String posCategoryProducts(ModelMap model, HttpServletRequest request) {

        long org = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        Iterable<ShopProduct> shopProducts = shopProductService.findAll(org);

        ShoppingCart shoppingCart = (ShoppingCart) request.getSession().getAttribute(BVMicroUtils.SHOPPING_CART);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
        }
        request.getSession().setAttribute(BVMicroUtils.SHOPPING_CART, shoppingCart);

        model.put("shopProducts", shopProducts);
        model.put("categories", productCategoryService.findAll(org));
        return "posCategoryProducts";
    }

    @PostMapping(value = "/registerShopProductForm")
    public String registerSavingForm(@ModelAttribute("shopProduct") ShopProduct shopProduct,
                                     ModelMap model, HttpServletRequest request) {
        long org = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        shopProduct.setOrgId(org);
        shopProductService.save(shopProduct);
        model.put("warehouses", warehouseLocationService.findAll(org));
        model.put("categories", productCategoryService.findAll(org));
        model.put("shopProduct", shopProduct);
        model.put("shopProductInfo", "Successfully created " + shopProduct.getName());
        return "posShopProduct";
    }

    @GetMapping(value = "/registerShopProduct")
    public String registerSaving(ModelMap model, HttpServletRequest request) {
        long org = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);

        model.put("shopProduct", new ShopProduct());
        model.put("warehouses", warehouseLocationService.findAll(org));
        model.put("categories", productCategoryService.findAll(org));
        return "posShopProduct";
    }

    @PostMapping(value = "/registerWarehouseLocationForm")
    public String registerWarehouseLocationForm(@ModelAttribute("warehouseLocation") WarehouseLocation warehouseLocation,
                                                ModelMap model, HttpServletRequest request) {
        model.put("warehouseLocationInfo", "Successfully created " + warehouseLocation.getName());
        warehouseLocationService.save(warehouseLocation);
        return "posWarehouseLocation";
    }

    @GetMapping(value = "/registerWarehouseLocation")
    public String registerWarehouseLocation(ModelMap model, HttpServletRequest request) {
        long org = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        model.put("warehouseLocation", new WarehouseLocation());
        model.put("warehouses", warehouseLocationService.findAll(org));
        return "posWarehouseLocation";
    }

    @PostMapping(value = "/registerProductCategoryForm")
    public String registerProductCategoryForm(@ModelAttribute("productCategory") ProductCategory productCategory,
                                              ModelMap model, HttpServletRequest request) {
        model.put("productCategoryInfo", "Successfully created " + productCategory.getName());
        productCategory.setOrgId((Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG));
        productCategoryService.save(productCategory);
        return "productCategory";
    }

    @GetMapping(value = "/registerProductCategory")
    public String registerProductCategory(ModelMap model, HttpServletRequest request) {
        long org = (Long) request.getSession().getAttribute(BVMicroUtils.CURRENT_ORG);
        model.put("productCategory", new ProductCategory());
        model.put("productCategories", productCategoryService.findAll(org));
        return "productCategory";
    }

    @PostMapping(value = "/checkoutShoppingCart")
    public String checkoutShoppingCart(ModelMap model, HttpServletRequest request) {
        ShoppingCart shoppingCart = (ShoppingCart) request.getSession().getAttribute(BVMicroUtils.SHOPPING_CART);
        shoppingCart = shopProductService.checkoutShoppingCart(shoppingCart);

//        model.put("productCategory", new ProductCategory() );
        request.getSession().setAttribute(BVMicroUtils.SHOPPING_CART, new ShoppingCart());
        model.put("amount", shoppingCart.getTotal());
        return "ccPaymentResult";
    }

}
package com.bitsvalley.micro.controllers;

import com.bitsvalley.micro.domain.*;
import com.bitsvalley.micro.repositories.CurrentAccountRepository;
import com.bitsvalley.micro.repositories.PaymentTransactionRepository;
import com.bitsvalley.micro.services.*;
import com.bitsvalley.micro.utils.BVMicroUtils;
import com.bitsvalley.micro.utils.CollectionRequestStatus;
import com.bitsvalley.micro.webdomain.ChargeRequest;
import com.bitsvalley.micro.webdomain.RuntimeSetting;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

@Controller
public class PaymentController extends SuperController{

    @Autowired
    private StripeService paymentsService;

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private SavingAccountService savingAccountService;

    @Autowired
    private CurrentAccountRepository currentAccountRepository;

    @Autowired
    private InitSystemService initSystemService;

    @Autowired
    CMRService cmrService;

    @Autowired
    private CurrentAccountService currentAccountService;


    @PostMapping("/charge")
    public String charge(@ModelAttribute ChargeRequest chargeRequest, HttpServletRequest request, Model model) {

//        String description = request.getParameter("description");
        String accountType = request.getParameter("accountType");
        String bid = request.getParameter("bid");

        chargeRequest.setCurrency(ChargeRequest.Currency.USD);
        chargeRequest.setDescription( bid + " - "+ chargeRequest.getDescription());
        Charge charge = null;
        boolean publicUser = false;
        try {
            charge = paymentsService.charge( chargeRequest);
            float moneyString = charge.getAmount();

            String amountFormat = String.format("%.2f", moneyString/100);
            model.addAttribute("amount", (amountFormat));
        model.addAttribute("id", charge.getId());
        model.addAttribute("status", charge.getStatus()+"! Your "+accountType+" will be credited");
        model.addAttribute("chargeId", charge.getId());
        model.addAttribute("balance_transaction", charge.getBalanceTransaction());

            String loggedInUserName = getLoggedInUserName();
            if(loggedInUserName.indexOf("anonymousUser") > -1){
                loggedInUserName = bid;
                publicUser = true;
            }

        paymentsService.updateStripePaymentTransaction(chargeRequest, charge, loggedInUserName);

        } catch (StripeException e) {
            model.addAttribute("error", e.getMessage());
            e.printStackTrace();
        }
        // Update Account and General ledger
        if(publicUser){
            RuntimeSetting byBid = initSystemService.findByBid(bid);
            model.addAttribute("businessInfo",byBid);
            return "ccPublicPayResult";
        }

        return "ccPaymentResult";
    }


    @PostMapping("/chargeCCSaving")
    public String chargeCCSaving(@ModelAttribute ChargeRequest chargeRequest, HttpServletRequest request, Model model) {

        String accountType = request.getParameter("accountType");
        String accountId = request.getParameter("accountId");


        //Idealy Carry transaction in DB than session. Safer
        SavingAccountTransaction ccSavingAccountTransaction = (SavingAccountTransaction)request.getSession().getAttribute("ccSavingAccountTransaction");
        RuntimeSetting runtimeSetting = (RuntimeSetting) request.getSession().getAttribute("runtimeSettings");

//        CurrentAccount byId = currentAccountRepository.findById(new Long(accountId)).get();
        chargeRequest.setDescription(accountType+" payment " );

        chargeRequest.setCurrency(ChargeRequest.Currency.USD);
        Charge charge = null;
        try {
            charge = paymentsService.charge(chargeRequest);
            model.addAttribute("amount", "$"+(charge.getAmount()));
            model.addAttribute("id", charge.getId());
            model.addAttribute("status", charge.getStatus()+"! Your "+accountType+" will be credited");
            model.addAttribute("chargeId", charge.getId());
            model.addAttribute("balance_transaction", charge.getBalanceTransaction());

            paymentsService.updateStripePaymentTransaction(chargeRequest, charge, getLoggedInUserName());
//            savingAccountTransactionRepository.save(ccSavingAccountTransaction);
            SavingAccount byId = savingAccountRepository.findById(ccSavingAccountTransaction.getSavingAccount().getId()).get();
            ccSavingAccountTransaction.setSavingAccount(byId);

            savingAccountService.createSavingAccountTransaction(ccSavingAccountTransaction, byId, runtimeSetting);

        } catch (StripeException e) {
            model.addAttribute("error", e.getMessage());
            e.printStackTrace();
        }
        // Update Account and General ledger
        return "ccPaymentResult";
    }



    @ExceptionHandler(StripeException.class)
    public String handleError(Model model, StripeException ex) {
        model.addAttribute("error", ex.getMessage());
        return "ccPaymentResult";
    }


    @PostMapping(value = "/sendCurrentToMomo")
    public String sendCurrentToMomo(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException {

        User user = (User) request.getSession().getAttribute(BVMicroUtils.CUSTOMER_IN_USE);
        int momoAmount = Integer.parseInt(request.getParameter("momoAmount"));
        String momoNumber = user.getTelephone1();


//        CurrentAccount currentAccount = currentAccountRepository.findById(Long.parseLong(request.getParameter("currentAccountId"))).get();
////        cmrService.sendMomoRequest()
//        CurrentAccountTransaction currentAccountTransaction = new CurrentAccountTransaction();
//        currentAccountTransaction.setModeOfPayment("MOMO");
//        currentAccountTransaction.setWithdrawalDeposit(1);
//        currentAccountTransaction.setCurrentAmount(momoAmount);
//        currentAccountTransaction.setNotes("Self service momo transfer");

//        currentAccountService.createCurrentAccountTransaction(currentAccountTransaction,currentAccount);
        // check balances and show confirmation page
        // or show inline error message from origin page
        return "userHomeCustomer";
    }


    @PostMapping(value = "/receiveCurrentFromMomo")
    public String receiveCurrentFromMomo(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = (User) request.getSession().getAttribute(BVMicroUtils.CUSTOMER_IN_USE);
        String amount = request.getParameter("momoAmount");
        int momoAmount = Integer.parseInt(amount);
        String collectionPhoneNumer = user.getTelephone1().replaceAll(" ","");

        CollectionRequestStatus collectionRequestStatus = cmrService.sendMomoCollectionRequest(new BigDecimal(momoAmount), Long.parseLong(collectionPhoneNumer));
        // check balances and show confirmation page
        // or show inline error message from origin page

        return "userHomeCustomer";
    }

}

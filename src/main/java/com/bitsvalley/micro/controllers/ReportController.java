package com.bitsvalley.micro.controllers;

import com.bitsvalley.micro.domain.User;
import com.bitsvalley.micro.repositories.LedgerAccountRepository;
import com.bitsvalley.micro.repositories.UserRepository;
import com.bitsvalley.micro.services.ReportService;
import com.bitsvalley.micro.webdomain.GLSearchDTO;
import com.bitsvalley.micro.webdomain.RuntimeSetting;
import com.bitsvalley.micro.webdomain.SavingReportDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ReportController extends SuperController {

    @Autowired
    ReportService reportService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    LedgerAccountRepository ledgerAccountRepository;

    @GetMapping(value = "/reports")
    public String reports(ModelMap model, HttpServletRequest request) {

        User user = userRepository.findByUserName(getLoggedInUserName());
        RuntimeSetting runtimeSetting = (RuntimeSetting) request.getSession().getAttribute("runtimeSettings");

//        SavingReportDTO savingReportDTO = reportService.savingReports(getLoggedInUserName(), runtimeSetting.getCountryCode());
//        GLSearchDTO glSearchDTO = new GLSearchDTO();
//        model.put("allLedgerAccount", ledgerAccountRepository.findByOrgIdAndActiveTrue(user.getOrgId()));
//        model.put("glSearchDTO", glSearchDTO);
//
//        model.put("descriptionValues", savingReportDTO.getDescriptionValues());
//        model.put("paidValues", savingReportDTO.getPaidValues());
//        model.put("dueValues", savingReportDTO.getDueValues());
//        model.put("sumTotalPaid", savingReportDTO.getSumTotalPaid());
//        model.put("sumTotalDue", savingReportDTO.getSumTotalDue());
        return "reports";

    }


    @GetMapping(value = "/filterReports")
    public String filterReports(ModelMap model, HttpServletRequest request) {

//        User user = userRepository.findByUserName(getLoggedInUserName());
//        SavingReportDTO savingReportDTO = reportService.savingReports(getLoggedInUserName());
//        GLSearchDTO glSearchDTO = new GLSearchDTO();
//        model.put("allLedgerAccount", ledgerAccountRepository.findByOrgIdAndActiveTrue(user.getOrgId()));
//        model.put("glSearchDTO", glSearchDTO);
//
//        model.put("descriptionValues", savingReportDTO.getDescriptionValues());
//        model.put("paidValues", savingReportDTO.getPaidValues());
//        model.put("dueValues", savingReportDTO.getDueValues());
//        model.put("sumTotalPaid", savingReportDTO.getSumTotalPaid());
//        model.put("sumTotalDue", savingReportDTO.getSumTotalDue());
        return "reports";

    }

}

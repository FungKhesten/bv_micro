package com.bitsvalley.micro.controllers;

import com.bitsvalley.micro.repositories.GeneralLedgerRepository;
import com.bitsvalley.micro.repositories.LedgerAccountRepository;
import com.bitsvalley.micro.repositories.UserRepository;
import com.bitsvalley.micro.services.GeneralLedgerService;
import com.bitsvalley.micro.services.LedgerAccountService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Fru Chifen
 * 11.06.2021
 */
@Controller
public class ExcelController extends SuperController {

    @Autowired
    GeneralLedgerRepository generalLedgerRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    LedgerAccountRepository ledgerAccountRepository;

    @Autowired
    LedgerAccountService ledgerAccountService;

    @Autowired
    GeneralLedgerService generalLedgerService;

    @PostMapping("/import")
    public void mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile) throws IOException {
//
//        User user = userRepository.findByUserName(getLoggedInUserName());
////        List<Test> tempStudentList = new ArrayList<Test>();
//        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
//        XSSFSheet worksheet = workbook.getSheetAt(0);
//        int j = 0;
//        //Run to Create ledger Accounts
//        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
//            XSSFRow row = worksheet.getRow(i);
////            Date dateCellValue = row.getCell(1).getDateCellValue();
//            String effectiveDate = row.getCell(2).getRawValue();
//            effectiveDate = effectiveDate.substring(0, 4)+"-"+effectiveDate.substring(4, 6) + "-01" ;
//
//            String notes = row.getCell(3).getStringCellValue();
//            String ledgerName = row.getCell(4).getStringCellValue();
//            String methodOfPayment = row.getCell(5).getStringCellValue();
//            double income = row.getCell(6).getNumericCellValue();
//            double expense = row.getCell(7).getNumericCellValue();
//            double amount = income == 0 ? expense : income;
//            LedgerAccount ledgerAccount = ledgerAccountRepository.findByNameAndOrgId(ledgerName, user.getOrgId());
//            LedgerEntryDTO ledgerEntryDTO = new LedgerEntryDTO();
//
//            if (ledgerAccount == null) {
//                if(income>0) {
//                    ledgerAccount = ledgerAccountService.createLedger(user.getOrgId(), ledgerName, "true", "3000 – 3999");
//                    ledgerEntryDTO.setCreditOrDebit(BVMicroUtils.CREDIT);
//                    ledgerEntryDTO.setLedgerAmount(amount);
//                }
//                else {
//                    ledgerAccount = ledgerAccountService.createLedger(user.getOrgId(), ledgerName, "false", "6000 – 6999");
//                    ledgerEntryDTO.setCreditOrDebit(BVMicroUtils.DEBIT);
//                    ledgerEntryDTO.setLedgerAmount(-1*amount);
//                }
//            } else {
//                if(income>0) {
//                    ledgerEntryDTO.setCreditOrDebit(BVMicroUtils.CREDIT);
//                    ledgerEntryDTO.setLedgerAmount(amount);
//                }
//                else {
//                    ledgerEntryDTO.setCreditOrDebit(BVMicroUtils.DEBIT);
//                    ledgerEntryDTO.setLedgerAmount(-1*amount);
//                }
//            }
//            ledgerEntryDTO.setRecordDate(effectiveDate);
//            ledgerEntryDTO.setOrgId(user.getOrgId());
//            ledgerEntryDTO.setNotes(methodOfPayment + " - " + notes);
//            ledgerEntryDTO.setOriginLedgerAccount(ledgerAccount.getId());
//
//            LedgerAccount byNameAndOrgId = ledgerAccountRepository.findByNameAndOrgId(BVMicroUtils.CASH, user.getOrgId());
//            ledgerEntryDTO.setDestinationLedgerAccount(byNameAndOrgId.getId());
//            generalLedgerService.updateManualAccountTransaction(ledgerEntryDTO, true);
//        }
//        //Run to create transactions
//

    }

}
